.. Tutoriel Docker documentation master file, created by
   sphinx-quickstart on Fri Jan 12 10:55:29 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


.. figure:: images/Docker_logo.svg.png
   :align: center

   Logo Docker https://www.docker.com


.. _tuto_docker:

================
Tutoriel Docker
================

- https://gitlab.com/gdevops/tuto_docker
- https://gdevops.gitlab.io/tuto_docker/
- https://fr.wikipedia.org/wiki/Docker_%28logiciel%29
- https://container.training/
- https://docs.docker.com/
- https://twitter.com/Docker/lists/docker-captains/members
- https://twitter.com/docker
- https://twitter.com/MirantisIT
- http://training.play-with-docker.com/
- https://avril2018.container.training/
- https://github.com/jpetazzo/container.training
- https://jpetazzo.github.io/2018/03/28/containers-par-ou-commencer/


.. toctree::
   :maxdepth: 5

   news/news
   installation/installation
   commands/commands
   documentation/documentation
   glossaire/glossaire
   versions/versions

.. toctree::
   :maxdepth: 5

   meta/meta
