

.. _dockerswarm_rocks_2019_01_01:

===========================
https://dockerswarm.rocks/
===========================

.. seealso::

   - https://dockerswarm.rocks/
   - :ref:`docker_swarm_rocks`
