
.. index::
   pair: News ; 2017-08


.. _news_2017_08_04:

==========================================================
4 août 2017 "Docker et Shorewall" par Guillaume Cheramy
==========================================================



.. seealso::

   - https://www.guillaume-cheramy.fr/docker-et-shorewall/
   - https://twitter.com/cheramy_linux



Créer les règles shorewall pour Docker
=========================================

Il faut créer dans shorewall les règles pour que les conteneurs puissent
avoir accès à Internet :
