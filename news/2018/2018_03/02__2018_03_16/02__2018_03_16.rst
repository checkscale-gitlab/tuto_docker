
.. index::
   pair: Docker ; registry


.. _2018_03_16:

===================================================================================
Jeudi 29 mars 2018 : Running Your Own Registry
===================================================================================

.. seealso::

   - https://blog.sixeyed.com/windows-weekly-dockerfile-20-running-your-own-registry/


Docker Registry
=================

The registry is the "ship" part of the build, ship, run workflow.

You package your app in a Docker image using a Dockerfile and
docker image build, and the output is an image on your machine
(or the CI server that ran the build).
