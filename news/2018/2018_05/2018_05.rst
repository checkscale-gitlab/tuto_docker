
.. index::
   pair: News ; 2018-05
   pair: Lacey Williams ; 2018-05
   pair: Mickael Baron ; 2018-05


.. _news_2018_05:

===========================
Actions/news mai 2018
===========================




Tutoriel pour préparer son environnement de développement ROS avec Docker de Mickael Baron
============================================================================================

.. seealso::

   - :ref:`mickael_baron_tuto`


.. _video_lacey:

DjangoCon 2018 - An Intro to Docker for Djangonauts by Lacey Williams
=======================================================================

.. seealso::

   - https://www.youtube.com/watch?v=v5jfDDg55xs&feature=youtu.be&a=
   - :ref:`tuto_lacey`


hard-multi-tenancy-in-kubernetes
===================================

.. seealso::

   - https://blog.jessfraz.com/post/hard-multi-tenancy-in-kubernetes/

containers-security-and-echo-chambers
=======================================

.. seealso::

   - https://blog.jessfraz.com/post/containers-security-and-echo-chambers/



Aly Sivji, Joe Jasinski, tathagata dasgupta (t) - Docker for Data Science - PyCon 2018
========================================================================================

.. seealso::

   - https://github.com/docker-for-data-science/docker-for-data-science-tutorial
   - https://www.youtube.com/watch?v=jbb1dbFaovg
   - https://t.co/ZW7g1JY3va


Description
-------------

Jupyter notebooks simplify the process of developing and sharing Data
Science projects across groups and organizations.
However, when we want to deploy our work into production, we need to
extract the model from the notebook and package it up with the required
artifacts (data, dependencies, configurations, etc) to ensure it works
in other environments.

Containerization technologies such as Docker can be used to streamline
this workflow.

This hands-on tutorial presents Docker in the context of Reproducible
Data Science - from idea to application deployment.

You will get a thorough introduction to the world of containers; learn
how to incorporate Docker into various Data Science projects; and walk
through the process of building a Machine Learning model in Jupyter
and deploying it as a containerized Flask REST API.


Créez un cluster hybride ARM/AMD64 (GNU/Linux N°215 mai 2018)
================================================================
