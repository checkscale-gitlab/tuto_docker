
.. index::
   pair: News ; 2018-01-31
   pair: Export ; PostgreSQL


.. _03_2018_01_31:

================================================================================================
Mercredi 31 janvier 2018 : Bilan mardi 30 janvier 2018
================================================================================================


.. seealso::

   - :ref:`postgres_docker`
   - :ref:`02_2018_01_30`
   - :ref:`images_postgres`





Suppression de la base db_id3_intranet
=======================================


psql -U postgres
-----------------

::

    root@caa4db30ee94:/# psql -U postgres

::

	psql (10.1)
	Type "help" for help.


l
---

::

    postgres=# \l


::


										List of databases
		  Name       |  Owner   | Encoding |  Collate   |   Ctype    |   Access privileges
	-----------------+----------+----------+------------+------------+-----------------------
	 db_id3_intranet | id3admin | UTF8     | en_US.utf8 | en_US.utf8 |
	 postgres        | postgres | UTF8     | en_US.utf8 | en_US.utf8 |
	 template0       | postgres | UTF8     | en_US.utf8 | en_US.utf8 | =c/postgres          +
					 |          |          |            |            | postgres=CTc/postgres
	 template1       | postgres | UTF8     | en_US.utf8 | en_US.utf8 | =c/postgres          +
					 |          |          |            |            | postgres=CTc/postgres
	(4 rows)


drop database db_id3_intranet;
-------------------------------

::

    postgres=# drop database db_id3_intranet;

::

	DROP DATABASE



Bilan mardi 30 janvier 2018
==============================

Pour pouvoir importer une base données PostgreSQL, il faut utiliser
cette suite de commandes dans le fichier docker-compose.yml.

::

	version: "3"

	services:
	  db:
		image: postgres:10.1
		container_name: container_intranet
		volumes:
		  - volume_intranet:/var/lib/postgresql/data/
		  - .:/code

	volumes:
	  volume_intranet:


La commande **.:/code** permet de voir ce qu'il y a dans le répertoire
du coté *host*.

::

    root@caa4db30ee94:/# ls -als code

::

	total 33897
	4 drwxr-xr-x 2 root root     4096 Jan 31 08:24 .
	4 drwxr-xr-x 1 root root     4096 Jan 30 13:46 ..
	33776 -rwxr-xr-x 1 root root 34586512 Jan 25 13:51 db_id3_intranet_2018_01_25.sql
	1 -rwxr-xr-x 1 root root      214 Jan 30 13:46 docker-compose.yml
	24 -rwxr-xr-x 1 root root    23949 Jan 30 14:04 postgresql.rst
	8 -rwxr-xr-x 1 root root     6238 Jan 31 08:24 README.txt
	80 -rwxr-xr-x 1 root root    80802 Jan 22 12:03 stack_overflow_postgres.png


On voit bien le fichier **db_id3_intranet_2018_01_25.sql**


Pour lancer PostgreSQL
========================

::

    docker-compose up



.. figure:: docker_compose_up.png
   :align: center

   docker-compose up


Pour accéder au conteneur
===========================


docker ps
----------

::

    docker ps

::

    PS Y:\projects_id3\P5N001\XLOGCA135_tutorial_docker\tutorial_docker\tutoriels\postgresql> docker ps

::

	CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS               NAMES
	caa4db30ee94        postgres:10.1       "docker-entrypoint.s…"   19 hours ago        Up 34 minutes       5432/tcp            container_intranet


docker exec -ti caa4db30ee94 bash
-----------------------------------

::


	PS Y:\projects_id3\P5N001\XLOGCA135_tutorial_docker\tutorial_docker\tutoriels\postgresql> docker exec -ti caa4db30ee94 bash

::


	root@caa4db30ee94:/# ls -als
	total 80
	4 drwxr-xr-x   1 root root 4096 Jan 30 13:46 .
	4 drwxr-xr-x   1 root root 4096 Jan 30 13:46 ..
	4 drwxr-xr-x   1 root root 4096 Dec 12 06:04 bin
	4 drwxr-xr-x   2 root root 4096 Nov 19 15:25 boot
	4 drwxr-xr-x   2 root root 4096 Jan 31 08:22 code
	0 drwxr-xr-x   5 root root  340 Jan 31 07:46 dev
	4 drwxr-xr-x   2 root root 4096 Dec 12 06:04 docker-entrypoint-initdb.d
	0 lrwxrwxrwx   1 root root   34 Dec 12 06:05 docker-entrypoint.sh -> usr/local/bin/docker-entrypoint.sh
	0 -rwxr-xr-x   1 root root    0 Jan 30 13:46 .dockerenv
	4 drwxr-xr-x   1 root root 4096 Jan 30 13:46 etc
	4 drwxr-xr-x   2 root root 4096 Nov 19 15:25 home
	4 drwxr-xr-x   1 root root 4096 Dec 10 00:00 lib
	4 drwxr-xr-x   2 root root 4096 Dec 10 00:00 lib64
	4 drwxr-xr-x   2 root root 4096 Dec 10 00:00 media
	4 drwxr-xr-x   2 root root 4096 Dec 10 00:00 mnt
	4 drwxr-xr-x   2 root root 4096 Dec 10 00:00 opt
	0 dr-xr-xr-x 132 root root    0 Jan 31 07:46 proc
	4 drwx------   1 root root 4096 Jan 30 14:32 root
	4 drwxr-xr-x   1 root root 4096 Dec 12 06:05 run
	4 drwxr-xr-x   1 root root 4096 Dec 12 06:04 sbin
	4 drwxr-xr-x   2 root root 4096 Dec 10 00:00 srv
	0 dr-xr-xr-x  13 root root    0 Jan 31 07:46 sys
	4 drwxrwxrwt   1 root root 4096 Jan 30 13:46 tmp
	4 drwxr-xr-x   1 root root 4096 Dec 10 00:00 usr
	4 drwxr-xr-x   1 root root 4096 Dec 10 00:00 var



Livre *PostgreSQL : Administration et exploitation de vos bases de données*
============================================================================

De nombreuses informations **très intéressantes**.

- psql -f nom_fichier.sql
- explications sur les bases template0 et template1
