
.. index::
   pair: News ; 2018-01-30


.. _02_2018_01_30:

========================================================================================
Mardi 30 janvier 2018 : écriture des fichiers **Dockerfile** et **docker-compose.yml**
========================================================================================


.. seealso::

   - :ref:`postgres_docker`
   - :ref:`03_2018_01_31`





Objectifs pour la journée
==========================

Mises et point et premières exécutions.

Dans un premier temps on ne prend pas en charge les secrets.


Avancement, découverte
=======================

- je repasse sur le tutoriel :ref:`postgresql <postgres_docker>` pour
  essayer de comprendre les volumes.



Historique
===========

- ajout MISC95


::

	CREATE DATABASE db_test WITH OWNER = id3admin ENCODING = 'UTF8' CONNECTION LIMIT = -1;


	C:\Tmp>psql -U postgres < create_database.sql
	Mot de passe pour l'utilisateur postgres : id338

	CREATE DATABASE
