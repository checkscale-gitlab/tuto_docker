
.. index::
   pair: News ; 2018-01-31

.. _04_2018_01_31:

================================================================================================
Mercredi 31 janvier 2018 : export/import d'une base de données PostgreSQL (tutoriel PostgreSQL)
================================================================================================


.. seealso::

   - :ref:`import_export_postgres`


Dockerfile
===========

::

	FROM postgres:10.1
	RUN localedef -i fr_FR -c -f UTF-8 -A /usr/share/locale/locale.alias fr_FR.UTF-8
	ENV LANG fr_FR.utf8


docker-compose.yml
====================

::

	version: "3"
	services:
	  db:
		build:
		  context: .
		  dockerfile: Dockerfile
		image: postgres:10.1
		container_name: container_intranet
		volumes:
		  - volume_intranet:/var/lib/postgresql/data/
		  - .:/code

	volumes:
	  volume_intranet:

Export
=======

- pg_dump -U postgres --clean --create -f db.dump.sql db_id3_intranet


Import
=======


- psql -U postgres -f db.dump.sql


Commandes docker-compose
=========================

- docker-compose up
- docker-compose down
- docker-compose exec db bash
