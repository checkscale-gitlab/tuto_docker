
.. index::
   pair: Jérôme Petazzoni ; 2020-04-07
   pair: Docker; Nixery
   pair: Docker; Nix
   pair: Docker ; timezone


.. _jpetazzo_2020_04_07:

=================================================================================================
**Nix de quoi s'agit-il ?**  by Jérôme Petazzoni
=================================================================================================

.. seealso::

   - https://enix.io/fr/blog/cherie-j-ai-retreci-docker-part3/
   - https://www.ardanlabs.com/blog/2020/04/docker-images-part3-going-farther-reduce-image-size.html
   - :ref:`kub_jpetazzo_2020_04_15`
   - :ref:`jpetazzo_2020_04_15`
   - https://nixery.dev/
   - https://github.com/google/nixery
   - https://www.youtube.com/watch?v=UbXv-T4IUXk
   - https://twitter.com/nixos_org
   - http://2010.rmll.info/NixOS-The-Only-Functional-GNU-Linux-Distribution.html
   - :ref:`docker_jpetazzo`
   - https://fr.slideshare.net/datakurre/nix-for-python-developers
   - https://github.com/NixOS
   - https://github.com/NixOS/nix
   - https://github.com/NixOS/nixpkgs
   - https://github.com/NixOS/nixos-weekly
   - https://github.com/NixOS/nixos-weekly/pulls
   - https://grahamc.com/blog/nix-and-layered-docker-images
   - https://www.youtube.com/watch?v=pOI9H4oeXqA
   - https://twitter.com/ellenkorbes/status/1216458929636630533
   - https://twitter.com/ellenkorbes
   - https://twitter.com/delabassee
   - https://twitter.com/sylr
   - https://twitter.com/gleber
   - https://twitter.com/tazjin
   - https://twitter.com/s0ulshake
   - https://twitter.com/brimstone75
   - https://twitter.com/rdegez
   - https://twitter.com/enixsas




Version anglaise originale
============================

.. seealso::

   - https://www.ardanlabs.com/blog/2020/04/docker-images-part3-going-farther-reduce-image-size.html

Vidéo
======

.. seealso::

   - :ref:`jpetazzo_2020_04_15`


Origine: Ellen Korbes (https://twitter.com/ellenkorbes)
============================================================

.. seealso::

   - https://twitter.com/ellenkorbes/status/1216458929636630533
   - https://twitter.com/ellenkorbes
   - http://ellenkorbes.com/#talks

Nix
=====

.. seealso::

   - https://fr.slideshare.net/datakurre/nix-for-python-developers
   - https://github.com/NixOS
   - https://github.com/NixOS/nix
   - https://github.com/NixOS/nixpkgs
   - https://nixos.org/nixpkgs/manual/#chap-language-support
   - https://github.com/NixOS/nixos-weekly
   - https://github.com/NixOS/nixos-weekly/pulls

Après la publication des parties 1 et 2 de la série, plusieurs personnes
sont venues me voir pour évoquer avec enthousiasme le gestionnaire de
package Nix.

J’ai donc décidé de me pencher dessus et de lui consacrer une section
entière dans cette dernière partie.

Spoiler alert : oui, Nix peut vraiment vous aider à améliorer vos builds,
mais la courbe d’apprentissage est raide.
Peut-être pas autant que pour Bazel, mais pas loin. Vous allez devoir
apprendre Nix, ses concepts, son langage d’expression dédié, puis bien
sûr comment l’utiliser pour packager votre code dans votre langage et
votre framework préférés (jetez un œil au `manuel des nixpkgs`_ pour voir
à quoi ça ressemble).

Cela étant dit, j’ai quand même envie de vous parler de Nix en détail,
pour deux raisons : ses concepts centraux sont très puissants (et peuvent
nous donner de bonnes idées sur le packaging de nos applications de façon
plus générale), et il existe un projet particulier appelé Nixery_ qui sera
un ajout utile à notre arsenal (qu’on décide d’utiliser Nix ou pas).


.. _`manuel des nixpkgs`: https://nixos.org/nixpkgs/manual/#chap-language-support
.. _Nixery: https://nixery.dev/


Nix, de quoi s’agit-il ?
=========================

.. seealso::

   - http://2010.rmll.info/NixOS-The-Only-Functional-GNU-Linux-Distribution.html

J’ai entendu parler de Nix pour la première fois il y a en gros 10 ans,
alors que j’assistais à ce `talk aux RMLL`_.
À l’époque, c’était déjà très solide.
Autrement dit, on n’est pas du tout sur un truc de hipster avec rien
derrière.

Commençons par un peu de terminologie :

- Nix est un package manager à installer sur n’importe quelle machine
  Linux ou macOS ;
- NixOS est une distribution Linux basée sur Nix ;
- nixpkgs est une collection de packages pour Nix ;
- Une “dérivation” est une recette de build avec Nix.

**Nix est un package manager fonctionnel**.

“Fonctionnel” signifie que chaque package est défini par ses *inputs*
(code source, dépendances...) et par sa dérivation (recette de build).

Rien d’autre.

Si on utilise les mêmes inputs avec la même dérivation, on aura toujours
le même output.
À l’inverse, à chaque fois qu’on modifie quelque chose sur un des inputs
(mise à jour du code source, modification des dépendances), ou alors sur
la dérivation, l’output est différent. Ca parait logique, non ?
Et si ça vous rappelle le cache de build Docker, c’est normal… c’est
exactement la même idée !

Sur un système traditionnel, quand un package dépend d’un autre, la
dépendance est généralement décrite de manière souple, non stricte.

Par exemple, sur Debian, python3.8 dépend de python3.8-minimal (= 3.8.2-1),
qui dépend lui même de libc6 (>= 2.29).
En parallèle, ruby2.5 dépend de libc6 (>= 2.17). Du coup, on installe
une seule version de libc6 et généralement ça fonctionne.

Sur Nix, les package dépendent des versions exactes de ces bibliothèques
et un mécanisme très malin permet à chaque programme d’utiliser son
propre ensemble de bibliothèques sans conflit avec les autres.
(Si vous vous demandez comment ça marche : les programmes s’appuient sur
un linker configuré pour utiliser les bibliothèques à des chemins
spécifiques.
Conceptuellement, c’est comme lorsqu’on spécifie #!/usr/local/bin/my-custom-python-3.8
pour lancer un script Python avec une version particulière de l’interpréteur.)

Par exemple, quand un programme utilise la bibliothèque C, sur un système
classique le chemin serait /usr/lib/libc.so.6, alors qu’avec Nix, ça
pourrait être /nix/store/6yaj...drnn-glibc-2.27/lib/libc.so.6.

Vous voyez ce chemin /nix/store ? C’est le Nix store (étonnant !).

Nix y stocke des fichiers et répertoires immuables, chacun identifié par
son hash.
Ça se rapproche des layers utilisés par Docker, avec une grosse
différence : les layers Docker s’appliquent les uns par dessus les autres,
alors que les fichiers et répertoires de Nix sont totalement disjoints,
il n’y a jamais aucun conflit entre eux (puisque chaque objet est stocké
dans un répertoire séparé). Ce détail aura son importance plus tard.

Sous Nix, lorsqu’on veut installer un package, on télécharge en fait un
certain nombre de fichiers et de répertoires qu’on dépose dans
le Nix store puis on configure un profil (en gros, c’est un paquet de
liens symboliques pour que les programmes qu’on installe soient accessibles
depuis notre variable $PATH).


.. _`talk aux RMLL`: http://2010.rmll.info/NixOS-The-Only-Functional-GNU-Linux-Distribution.html

Passons à l’expérimentation de Nix !
=======================================

Jusqu’à présent, on était dans la théorie. Voyons maintenant Nix en action !

On peut lancer Nix dans un conteneur avec::

    docker run -ti nixos/nix

Puis on peut lister les packages installés avec les commandes::

    nix-env --query

ou::

    nix-env -q

À ce stade, on ne voit que nix et nss-cacert. Ça peut paraître bizarre :
où sont passés le shell et les outils habituels comme ls ou autres ?
Et bien dans cette image de conteneur, ils sont dans un exécutable
statique, appelé busybox.

Bon, on va peut-être installer quelque chose ?

On exécute::

    nix-env --install redis

ou::

    niv-env -i redis.

La sortie de cette commande nous montre qu’on récupère de nouveaux “paths”
et qu’ils sont stockés dans le store Nix.
On va avoir au moins un “path” pour redis lui-même et très probablement
un autre pour glibc.
Nix utilise lui aussi glibc (pour le binaire nix-env et quelques autres),
mais dans une version différente de celle utilisée par redis.

Si on lance ls -ld /nix/store/*glibc*/ on aura deux répertoires qui
correspondent aux deux versions de glibc.

Pendant que j’écris ces lignes, j’ai deux versions de glibc-2.27::


    ef5936ea667f:/# ls -ld /nix/store/*glibc*/

::

    dr-xr-xr-x    ... /nix/store/681354n3k44r8z90m35hm8945vsp95h1-glibc-2.27/
    dr-xr-xr-x    ... /nix/store/6yaj6n8l925xxfbcd65gzqx3dz7idrnn-glibc-2.27/

Vous allez me dire : “Attends, mais ce sont pas les mêmes versions ?”.

Et bien oui et non ! C’est bien le même numéro, mais ces versions ont
sûrement été construites avec des options ou des patchs légèrement
différents.
**Du point de vue de Nix, ce sont donc deux objets distincts**.

C’est exactement comme quand on build son programme avec le même Dockerfile
alors qu’on a modifié une ligne de code quelque part : le builder Docker
identifie ces petites différences et nous donne des images différentes.

On peut aussi demander à Nix de nous donner les dépendances de n’importe
quel fichier situé dans le store Nix.

Pour ça, on utilise les commandes::

    nix-store --query --references

ou::

    nix-store -qR

Par exemple, pour voir les dépendances des binaires redis qu’on vient
d’installer juste avant, on peut faire un::

   nix-store -qR $(which redis-server)

Et là, ça va être magique.

Si on veut lancer Redis, on a besoin seulement de ces répertoires.
Rien d’autre. Ça veut dire qu’on peut déposer ces répertoires dans
scratch, et ça marche. On n’a besoin d’aucune librairie supplémentaire.

On peut généraliser cette procédure en utilisant un profil Nix.
Un profil contient le répertoire bin à ajouter à notre $PATH (et quelques
autres trucs, mais je simplifie pour expliquer plus facilement).

Si on fait::

    nix-env --profile myprof -i redis memcached

le répertoire myprof/bin va donc contenir les exécutables pour Redis et
pour Memcached.

Encore mieux, les profils sont eux aussi stockés dans le store.
Du coup on peut lancer la commande::

    nix-store -qR

pour lister les dépendances d’un profil.


Créer des images minimales avec Nix
========================================

Si on met bout à bout tout ce que j’ai expliqué dans la section précédente,
on peut écrire le Dockerfile suivant::

    FROM nixos/nix
    RUN mkdir -p /output/store
    RUN nix-env --profile /output/profile -i redis
    RUN cp -va $(nix-store -qR /output/profile) /output/store
    FROM scratch
    COPY --from=0 /output/store /nix/store
    COPY --from=0 /output/profile/ /usr/local/

La première phase utilise Nix pour installer Redis dans un nouveau profil.

Ensuite on lui demande de lister toutes les dépendances de ce profil
(la commande nix-store -qR) et on copie ces dépendances dans /output/store.

La seconde phase copie ces dépendances dans l’image finale, dans
/nix/store (c.a.d. leur place d’origine dans Nix), ainsi que le profil
lui même. (Copier le profil nous permet de récupérer un répertoire bin
qui contient tous les binaires qu’on veut dans notre $PATH. Pratique.)

Le résultat est une image de 35Mo avec Redis et rien d’autre.

Si on veut aussi un shell, on peut simplement mettre à jour le
Dockerfile avec un -i redis bash, et voilà !

Si vous êtes tenté de ré-écrire tous vos Dockerfiles de cette façon là,
attendez un peu.
Déjà, cette image ne contient pas certaines metadata cruciales comme
VOLUME, EXPOSE, ou encore ENTRYPOINT et son wrapper associé.
Et surtout j’ai encore mieux pour vous dans la section suivante ...

Nixery
==========

.. seealso::

   - https://nixery.dev/
   - https://github.com/google/nixery
   - https://grahamc.com/blog/nix-and-layered-docker-images
   - https://www.youtube.com/watch?v=pOI9H4oeXqA

Tous les gestionnaires de package fonctionnent de la même façon : ils
téléchargent (ou génèrent) des fichiers et les installent sur notre
système.

Mais avec Nix, il y a une différence importante : les fichiers installés
sont immuables.
Quand on installe des packages avec Nix, ça n’a aucun impact sur ce
qu’on avait fait juste avant.

Les couches Docker peuvent s’affecter mutuellement (parce qu’une couche
peut changer ou supprimer un fichier de la couche précédente), mais ce
n’est pas le cas des objets stockés dans le Nix store.

Si on regarde le conteneur Nix qu’on a lancé un peu plus tôt (ou qu’on
en lance un nouveau avec docker run -ti nixos/nix), dans le répertoire
/nix/store, on va trouver une liste de répertoires comme ceux-là::

    b7x2qjfs6k1xk4p74zzs9kyznv29zap6-bzip2-1.0.6.0.1-bin/
    cinw572b38aln37glr0zb8lxwrgaffl4-bash-4.4-p23/
    d9s1kq1bnwqgxwcvv4zrc36ysnxg8gv7-coreutils-8.30/

Si on utilisait Nix pour construire une image (comme on l’a fait avec le
Dockerfile plus haut), on aurait juste besoin de ces répertoires dans
/nix/store et d’un paquet de liens symboliques.

Maintenant, imaginez qu’on stocke chaque répertoire du Nix store comme
un layer dans une registry Docker.

Ensuite, quand on a besoin d’une image avec les packages X, Y et Z, il suffit de :

- générer un petit layer avec les liens symboliques évoqués, permettant
  d’invoquer facilement les programmes contenus dans ces packages X, Y et Z
  (ce qui correspond à la dernière ligne COPY dans le Dockerfile précédent) ;
- demander à Nix quels sont les objets du store correspondant à X, Y et Z
  ainsi qu'à leurs dépendances ;
- générer un manifest d’image Docker qui référence tous ces layers.

**C’est exactement ce que Nixery fait**.

Nixery, c’est un registre de conteneurs “magique” : il génère à la volée
des manifests d’images de conteneurs qui référencent des layers qui
correspondent aux objets du Nix store.

Concrètement, si on fait un::

    docker run -ti nixery.dev/redis/memcached/bash bash

on obtient un shell dans un conteneur avec Redis, Memcached et Bash.

Ce qui est beau, c’est que l’image de ce conteneur est générée à la volée,
sans avoir besoin de la construire.
(Notez qu’on devrait plutôt faire un::

    docker run -ti nixery.dev/shell/redis/memcached sh

car quand une image commence par shell, Nixery nous donne quelques
packages essentiels en plus du shell, par exemple coreutils).

Nixery implémente quelques optimisations en prime (notamment pour gérer
la limite maximale de nombre de layers dans Docker).

Si vous êtes intéressé vous pouvez lire ce `blog post (1)`_ ou ce `talk de NixConf`_.

.. _`blog post (1)`: https://grahamc.com/blog/nix-and-layered-docker-images
.. _`talk de NixConf`:  https://www.youtube.com/watch?v=pOI9H4oeXqA


D’autres manières d’utiliser Nix
===================================

.. seealso::

   - https://lethalman.blogspot.com/2016/04/cheap-docker-images-with-nix_15.html

On peut aussi utiliser Nix directement pour générer des images de
conteneurs. Vous trouverez un bon exemple dans ce blog post.

Notez néanmoins que la technique évoquée ici nécessite kvm, ce qui veut
dire qu’elle ne fonctionnera pas dans des instances Cloud ou dans des
conteneurs (sauf en utilisant de la virtualisation imbriquée, mais
c’est encore très rare).

Apparemment, il faut adapter les exemples et utiliser buildLayeredImage
mais n’ayant pas investigué plus loin, je ne peux pas préciser la
quantité de travail et la complexité.

To Nix or not to Nix ?
=========================

Ce blog post ayant vocation à rester court (on me dit dans l’oreillette
que c’est raté😅), je n’ai pas la place d’expliquer en détail comment
utiliser Nix pour générer l’image parfaite.

Mais j’espère que cette petite démo de Nix vous permet de vous faire
une idée.

À vous d’approfondir le sujet si ça vous a ouvert l’appétit !

**Pour ma part, je compte bien utiliser Nixery quand j’ai besoin d’images
de conteneurs pour un usage précis, en particulier avec Kubernetes**.

Prenons un exemple, si j’ai besoin d’une image avec curl, tar et la CLI
de AWS.

Mon approche traditionnelle aurait été d’utiliser alpine puis d’exécuter
pip install awscli. Mais avec Nixery, je vais simplement pouvoir utiliser
l’image nixery.dev/shell/curl/gnutar/awscli !

::

    docker run -ti nixery.dev/shell/curl/gnutar/awscli sh


Avant de conclure, quelques détails importants
===============================================

Lorsqu’on utilise des images vraiment très réduites (comme scratch, ou
d’une certaine manière alpine, ou même certaines images générées avec
distroless, Bazel ou Nix), on peut être confronté à des erreurs
inattendues.

Il y a des fichiers auxquels on ne pense pas en général, mais qui sont
supposés se trouver sur tout système UNIX bien élevé, y compris dans
un conteneur.

De quels fichiers parle-t-on exactement ? Et bien, voici une petite liste non exhaustive :

- les certificats TLS ;
- les fichiers de timezone ;
- la base d’utilisateurs et groupes (UID/GID).

Voyons de quoi il s’agit plus précisément, de quand nous pouvons en
avoir besoin, et comment on peut les ajouter à nos images.

Les certificats TLS
====================

.. seealso::

   - https://twitter.com/sylr

Quand on établit une connection TLS vers un serveur distant (par exemple,
une requête vers un service web ou vers une API en HTTPS), en général il
nous présente son certificat.
En général, ce certificat a été signé par une autorité de certification
reconnue. En général, aussi, on veut vérifier que ce certificat est
valide et qu’on connait l’autorité qui l’a signé.

(Je dis “en général” car il y a de rares scénarios où on ça nous est é
gal, ou bien où l’on valide la connection différemment. Mais si vous êtes
dans un de ces cas-là, vous devez être au courant.
Sinon, par défaut, on part du principe qu’on doit valider les certificats !
Et oui, la sécurité avant tout !).

La clé (sans jeu de mot) de la vérification des certificats réside dans
ces fameuses autorités de certification. Pour valider les certificats
des serveurs auxquels on se connecte, on a besoin des certificats de
ces autorités. **Ils sont typiquement installés dans /etc/ssl**.

Si on utilise scratch ou une autre image minimale, et si on se connecte
à un serveur TLS, on peut obtenir des erreurs de validation des certificats.
En Go, ça ressemble à ce message d’erreur là x509: certificate signed
by unknown authority.
Si ça nous arrive, il suffit de rajouter les certificats dans notre image.
Pour ça, on peut les récupérer de quasiment toutes les images classiques
comme ubuntu ou alpine. Peu importe laquelle, les systèmes ont quasiment
tous le même groupe de certificats.


La ligne suivante va s’en occuper::

    COPY --from=alpine /etc/ssl /etc/ssl

On voit aussi que si on veut juste copier des fichiers depuis une image,
on peut utiliser --from pour se référer à cette image, sans référer à
un build stage particulier !


.. _timezone_docker:

Les fichiers de timezone
=============================

.. seealso::

   - https://twitter.com/sylr

Si notre code manipule la date et l’heure, en particulier l’heure locale
(par exemple, si on affiche l’heure dans un fuseau horaire particulier,
par opposition à une horloge interne), on a besoin des fichiers de
timezone.

Vous pourriez vous dire : “Attends, mais comment ça Jérôme ? Si je veux
manipuler les fuseaux horaires, j’ai seulement besoin d’un offset par
rapport à UTC ou GMT !”.

Oui, sauf que ... il y a les changements d’heure d'été et d’heure d’hiver.
Là, les choses se compliquent très vite, car différents pays ou régions
n’ont pas tous un passage à l’heure d'été, ou bien pas au même moment.

Par exemple, au sein même de l’Utah aux États-Unis, les règles changent
selon qu’on est en territoire Navajo ou pas !
Et puis par ailleurs, au fil des années, ces règles évoluent.
En Europe par exemple, le changement d’heure devrait être supprimé par
tous les pays en 2021.

Revenons-en à notre problématique technique.

Si on veut pouvoir afficher l’heure locale, on va avoir besoin des
fichiers qui décrivent ces informations. Sur UNIX, ce sont les fichiers
tzinfo ou zoneinfo, on les trouve généralement dans le répertoire **/usr/share/zoneinfo**.

Certaines images (par exemple centos ou debian) incluent nativement ces
fichiers de timezone.
D’autres non, comme alpine ou ubuntu, et le package qui inclut ces
fichiers s’appelle généralement tzdata.

Pour installer les fichiers de timezone dans notre image, on peut par
exemple lancer::

    COPY --from=debian /usr/share/zoneinfo /usr/share/zoneinfo

Ou si on utilise déjà alpine, on peut simplement faire un apk add tzdata.

Pour vérifier que les fichiers de timezone sont bien installés, on peut
faire cette commande dans notre conteneur::

    TZ=Europe/Paris date

Si on obtient quelque chose comme Fri Mar 13 21:03:17 CET 2020, on est
bon.

::

    $ TZ=Europe/Paris date

::


    dimanche 19 avril 2020, 11:35:01 (UTC+0200)


Si on obtient UTC, ça signifie que les fichiers de timezone n’ont pas
été trouvés.

Les fichiers de mapping UID/GID
====================================

.. seealso::

   - https://github.com/soulshake/clink
   - https://github.com/jessfraz/dockerfiles
   - https://twitter.com/jessfraz
   - https://medium.com/@chemidy/create-the-smallest-and-secured-golang-docker-image-based-on-scratch-4752223b7324
   - https://twitter.com/sylr


Une autre chose dont notre programme pourrait avoir besoin : rechercher
les identifiants système des utilisateurs (User ID / UID) et de groupes
d’utilisateurs (Group ID / GID).

Généralement, ça se passe dans **/etc/passwd et /etc/group**.

Le seul cas où j’ai été obligé de fournir ces fichiers explicitement,
c’était en exécutant des applications de bureau dans des conteneurs
(en utilisant des outils comme clink ou les dockerfiles de Jessica Frazelle.

Si vous avez besoin d’installer ces fichiers dans un conteneur, vous
pouvez les générer localement, ou dans un conteneur (single-stage ou
multistage), ou alors vous pouvez faire un bind-mount depuis l’hôte
(tout dépend de ce que vous êtes en train de faire).

Ce `blog post (2)`_ nous montre comment ajouter un utilisateur à un conteneur
de build, et ensuite copier les fichiers /etc/passwd et /etc/group au
conteneur de run.

.. _`blog post (2)`:  https://medium.com/@chemidy/create-the-smallest-and-secured-golang-docker-image-based-on-scratch-4752223b7324


Conclusions
================

Comme on a pu le voir, il existe plein de méthodes pour réduire la taille
de nos images. Vous vous demandez probablement laquelle est la meilleure
de façon absolue ? Et bien j’ai une mauvaise nouvelle : il n’y en a pas.

Comme toujours, la réponse est ça dépend.

Les multi-stage builds basés sur Alpine vont donner d’excellents résultats
dans de nombreux scénarios. Mais certaines bibliothèques ne vont pas
être disponibles sur Alpine, et les compiler à la main peut prendre du temps.

Dans ce cas un multi-stage build avec une distribution classique va faire
le job plus efficacement.

Les mécanismes comme Distroless ou Bazel peuvent apporter des résultats
encore meilleurs, mais ils vont souvent nécessiter un investissement
significatif en amont qui n’est pertinent que pour certaines organisations.

Les binaires statiques et l’image scratch peuvent être bien utiles
lorsqu’on déploie dans des environnements de très petite taille, comme
des systèmes embarqués par exemple.

Un dernier point important pour finir … Si on build et maintient de
nombreuses images (des centaines, voire plus), on peut vouloir s’en
tenir à une seule technique, même si elle n’est pas optimale dans
tous les cas.
C’est en effet souvent plus confortable de maintenir des centaines
d’images avec une structure similaire plutôt que d’avoir des pléthores
de variations avec des builds exotiques ou des Dockerfiles de niche.

On approche de la fin de ce tour d’horizon sur l’optimisation des
images Docker.
Avant de vous laisser, sachez que si vous utilisez des techniques que
je n’ai pas mentionnées dans cette série d’article, je serai ravi de
continuer à en discuter avec vous, alors contactez-moi et réparons
ces oublis !


Mot de fin et remerciements
==============================

.. seealso::

   - https://www.ardanlabs.com/blog/2020/04/docker-images-part3-going-farther-reduce-image-size.html
   - https://twitter.com/ellenkorbes/status/1216458929636630533
   - https://twitter.com/ellenkorbes
   - https://twitter.com/delabassee
   - https://twitter.com/sylr
   - https://twitter.com/gleber
   - https://twitter.com/tazjin
   - https://twitter.com/s0ulshake
   - https://twitter.com/brimstone75
   - https://twitter.com/rdegez

L’idée d’écrire cette série d’articles m’est venue en voyant ce tweet_
de @ellenkorbes (https://twitter.com/ellenkorbes).

Pendant mes formations sur les conteneurs, je passe toujours un peu de
temps à expliquer comment réduire la taille des images.
Pour évoquer le linking statique vs dynamique par exemple, je suis obligé
de prendre des raccourcis pour ne pas y passer la journée.
Ca laisse un peu un goût d’inachevé et m’a fait me demander s’il était
finalement si nécessaire d’évoquer tous ces détails. Puis quand j’ai vu
le tweet d’Ellen et certaines de vos réponses, je me suis dit :
“Wow, mais en fait ça pourrait aider pas mal de monde si j’écrivais
tout ce que j’ai pu expérimenter ces dernières années sur ces sujets !”.

… Je ne sais plus trop ce qui s’est passé ensuite, mais je me suis
réveillé à côté d’une caisse de Club Mate vide et de trois blog posts ! 🤷🏻

Si vous cherchez des ressources géniales pour faire tourner du Go sur
Kubernetes (et autres sujets connexes), je vous recommande vivement de
consulter la liste des talks proposée par Ellen.

La plupart de ces talks sont disponibles sur Youtube, et je vous promets
que c’est un très bon investissement de votre temps. En particulier, si
vous avez aimé cette série sur la taille des images Docker, vous pouvez
regarder son futur talk La quête du temps de déploiement le plus rapide.

Un grand merci aussi aux nombreuses personnes qui m’ont fait des retours
et des suggestions d’amélioration ! Je pense en particulier à :

- `David Delabassée`_ pour ses conseils sur Java et jlink ;
- `Sylvain Rabot`_ pour les certificats, les timezone et les fichiers UID et GID ;
- `Gleb Peregud`_ et `Vincent Ambo`_ pour le partage de ressources très
  utiles à propos de Nix.

Ces articles ont été écrits originalement en anglais.
La version anglaise a été relue par `AJ Bowen`_.

Elle a corrigé de nombreuses fautes de frappe et pas mal d’erreurs,
contribuant à améliorer significativement ce que j’avais écrit.
Toutes les erreurs restantes sont les miennes.

AJ travaille en ce moment sur un projet de préservation de cartes
postales anciennes et historiques. Si ça vous intéresse, je vous invite
vivement à vous inscrire ici_ pour en savoir plus.

La version française de cette série a été traduite par `Aurélien Violet`_
et par `Romain Degez`_. Si vous avez aimé cette version française, vous
pouvez leur témoigner un grand merci, ça a représenté bien plus de
travail qu’il n’y paraît !

Ne ratez pas nos prochains articles sur les conteneurs, le DevOps et le
Cloud Native ! Suivez Enix sur Twitter !


.. _tweet: https://twitter.com/ellenkorbes/status/1216458929636630533
.. _`David Delabassée`:  https://twitter.com/delabassee
.. _`Sylvain Rabot`: https://twitter.com/sylr
.. _`Gleb Peregud`: https://twitter.com/gleber
.. _`Vincent Ambo`: https://twitter.com/tazjin
.. _`AJ Bowen`: https://twitter.com/s0ulshake
.. _ici: https://www.ephemerasearch.com/
.. _`Aurélien Violet`: https://twitter.com/brimstone75
.. _`Romain Degez`: https://twitter.com/rdegez
