.. index::
   pair: Security; Clair
   pair: Security; Anchore

.. _martin_heinz_2020_04_01:

=========================================================================================
**Analyzing Docker Image Security** by Martin Heinz (https://twitter.com/Martin_Heinz\_)
=========================================================================================

.. seealso::

   - https://martinheinz.dev/blog/19
   - https://martinheinz.dev/
   - https://twitter.com/Martin_Heinz\_





Description
==============

A lot of people assume that Docker images and containers are secure by
default, which - unfortunately - is not the case.

There are quite a few things that effect security of your Docker images.

Whether it's packages installed in the image, libraries used by your
application or even the base image - all these components might introduces
vulnerability into your application.

A lot of these problems are easily avoidable, though...

Anchore & Clair
==================

The easiest way to find vulnerabilities in Docker images is to run
inspection against them using tools like Anchore or Clair:

Anchore Engine
----------------

Anchore is a centralized service for inspection,  analysis and certification
of container image. It scans images using vulnerability data (feeds)
from OS vendors like Red Hat, Debian or Alpine.

For non-OS data it uses NVD (National Vulnerability Database), which
includes vulnerabilities for RPM, Deb, APK as well as Python (PIP),
Ruby Gems, etc.

Clair
------

Clair is a static analyzer developed by CoreOS for Docker and APPC containers.

It uses vulnerability metadata from similar sources as Anchore - Red Hat
Security Data, NVD, Ubuntu CVE Tracker, Alpine SecDB, Debian Security
Bug Tracker, etc.


Setting Up
=============

Now that we know the tools we want to use, it's time spin them up.

Both Anchore and Clair include various integration and can be deployed
to Kubernetes or OpenShift, but for the purpose of this demonstration,
we will set them up using docker-compose on local machine:

To setup Anchore run following::

    mkdir ~/aevolume
    cd ~/aevolume

    docker pull docker.io/anchore/anchore-engine:latest
    docker create --name ae docker.io/anchore/anchore-engine:latest
    docker cp ae:/docker-compose.yaml ~/aevolume/docker-compose.yaml
    docker rm ae

    docker-compose pull
    docker-compose up -d

    export ANCHORE_CLI_USER=admin
    export ANCHORE_CLI_PASS=foobar

    docker run --net=host -e ANCHORE_CLI_URL=http://localhost:8228/v1/ -it anchore/engine-cli

To setup Clair run following::

    # Download Clair Scanner from https://github.com/arminc/clair-scanner/releases
    chmod +x clair-scanner

    docker run -p 5432:5432 -d --name db arminc/clair-db:$(date +%F)
    docker run -p 6060:6060 --link db:postgres -d --name clair arminc/clair-local-scan:v2.0.6

And with that we are ready to analyze !

.. note:: We will come back to Clair little later in the article.

Check Image for Vulnerabilities
=================================

Let's start with Anchore and basic Debian image.

What we need to do, to get our image analyzed is to add it and wait for
the analysis to complete:

# inside anchore-cli Docker container
------------------------------------------

::

    ~ $ anchore-cli image add docker.io/library/debian:latest

::

    Image Digest: sha256:121dd2a723be1c8aa8b116684d66157c93c801f2f5107b60287937e88c13ab89
    Parent Digest: sha256:a63d0b2ecbd723da612abf0a8bdb594ee78f18f691d7dc652ac305a490c9b71a
    Analysis Status: analyzed
    Image Type: docker
    Analyzed At: 2020-03-07T10:46:20Z
    Image ID: 971452c943760ab769134f22db8d3381b09ea000a6c459fbfa3603bb99115f62
    Dockerfile Mode: Guessed
    Distro: debian
    Distro Version: 10
    Size: 126607360
    Architecture: amd64
    Layer Count: 1

    Full Tag: docker.io/library/debian:latest
    Tag Detected At: 2020-03-07T10:45:48Z

::

    ~ $ anchore-cli image wait docker.io/library/debian:latest

::

    Status: analyzing
    Waiting 5.0 seconds for next retry.
    ...

::

    ~ $ anchore-cli image list

::

    Full Tag                                                      Image Digest                                                                   Analysis Status
    docker.io/library/debian:latest                               sha256:121dd2a723be1c8aa8b116684d66157c93c801f2f5107b60287937e88c13ab89        analyzed
