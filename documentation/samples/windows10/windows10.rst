.. index::
   pair: Samples ; Windows 10


.. _samples_windows_10:

=========================
Exemples sur Windows 10
=========================


.. seealso::

   - https://docs.microsoft.com/fr-fr/virtualization/windowscontainers/quick-start/quick-start-windows-10
