

.. index::
   pair: docker ; FAQ
   pair: Howto ;  run a shell in our running container ;
   pair: shell ;  container ;


.. _docker_faq:

======================================
docker FAQ
======================================

.. seealso::

   - https://docs.docker.com/compose/faq/




How to delete all your docker images ?
=========================================

::

    docker rm $(docker ps -a -q)



.. _go_inside_container:

How to run a shell in our running container ?
===============================================

.. seealso::

   - :ref:`inside_container`
   - :ref:`getting_shell_in_container`
   - :ref:`getting_shell_in_stopped`


There are 2 methods:

- :ref:`docker exec <docker_exec>`

  ::

      $ docker exec -ti ticktock sh


- overriding the Dockerfile entrypoint
  see https://avril2018.container.training/intro.yml.html#194

  ::

      $ docker run -it --entrypoint bash figlet


How to delete stopped containers ?
==================================

.. seealso::

   - :ref:`inside_container`

::

    $ docker system prune

::

	WARNING! This will remove:
			- all stopped containers
			- all networks not used by at least one container
			- all dangling images
			- all build cache
	Are you sure you want to continue? [y/N] y
	Deleted Containers:
	9a47c35465927f391fefd3faeec5b88a6926430ba7bf49160e08cfbf61d9aeab
	a1919f59bab55b472597c00051c5be57aac64e2f5d5e40deba0cbe5f9f4448ff
	49268904d59e18f3b4b33f1ff11122cc3d6cefc5dbec0a0242f20f4f2dee219f
	a061133b8ff0e07b63285573b2f3e4dc9ac598c36737d32c42ff0d80af7d5668

	Deleted Networks:
	ch4-message-board-app_default

	Deleted Images:
	deleted: sha256:e43bb6363c1ff911ce34c76475cfbc4020df989221710052a8be91f7702afcab
	deleted: sha256:46ee23e3a5a944b87b11ba03fda425d9b79a922c9df4e958def47785a5303965
	deleted: sha256:d373c573904be4c45edce0494c202f7a1cf44c87515ad24b2c2c80824b734115
	deleted: sha256:aee4f1ad67db567e681ed8847ab56c87489ab44bfd1cc183f9a75fc1164ce4a7
	deleted: sha256:724bf0a6facc9e4efd4e865c995a683e586981deb6310115269f864cda772836

	Total reclaimed space: 8.349kB


Where can I find example compose files ?
===========================================

There are many examples of Compose files on github.
