
.. index::
   pair: Hébergeurs ; Docker
   pair: Hébergeurs ; Gitlab


.. _hebergeurs_docker:

=====================================
Hébergeurs Docker
=====================================





Gitlab
=======

Gitlab peut héberger des images Docker.

Amazon
========

.. seealso::

   - http://www.journaldunet.com/solutions/cloud-computing/1205896-comment-aws-supporte-t-il-vraiment-docker/
