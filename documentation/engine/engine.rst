
.. index::
   pair: docker ; engine

.. _docker_engine_ce:

======================================
docker engine CE (Community Edition)
======================================

.. seealso::

   - https://github.com/docker/docker-ce
   - https://docs.docker.com/engine/deprecated/



docker swarm mode
=====================

.. toctree::
   :maxdepth: 3

   swarm/swarm
