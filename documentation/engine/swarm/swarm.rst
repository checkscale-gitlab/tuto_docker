
.. index::
   pair: docker ; swarm
   ! swarm
   ! docker swarm

.. _docker_swarm_mode:

======================================
docker swarm mode
======================================

.. seealso::

   - https://docs.docker.com/engine/swarm/



Docker swarm rocks
====================

.. seealso::

   - :ref:`docker_swarm_rocks`
