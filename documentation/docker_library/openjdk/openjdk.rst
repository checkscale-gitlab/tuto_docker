

.. index::
   pair: Images ; OpenJDK


.. _images_java:

==============================
Images **OpenJDK (Java)**
==============================


.. seealso::

   - https://hub.docker.com/_/openjdk/
   - https://en.wikipedia.org/wiki/OpenJDK
   - http://openjdk.java.net/
   - https://github.com/docker-library/openjdk
   - https://twitter.com/OpenJDK




.. figure:: openjdk_logo.png
   :align: center

   Le logo OpenJDK


.. figure:: images_openjdk.png
   :align: center

   https://hub.docker.com/_/openjdk/


Short Description
==================

OpenJDK is an open-source implementation of the Java Platform,
Standard Edition


What is OpenJDK ?
===================

.. seealso::

   - https://en.wikipedia.org/wiki/OpenJDK

OpenJDK (Open Java Development Kit) is a free and open source
implementation of the Java Platform, Standard Edition (Java SE).

OpenJDK is the official reference implementation of Java SE since
version 7.


How to use this image
=======================

Start a Java instance in your app

The most straightforward way to use this image is to use a Java container
as both the build and runtime environment.
In your Dockerfile, writing something along the lines of the following
will compile and run your project:

::

	FROM openjdk:7
	COPY . /usr/src/myapp
	WORKDIR /usr/src/myapp
	RUN javac Main.java
	CMD ["java", "Main"]

You can then run and build the Docker image:

::

	$ docker build -t my-java-app .
	$ docker run -it --rm --name my-running-app my-java-app
