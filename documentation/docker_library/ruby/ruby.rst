

.. index::
   pair: Images ; Ruby


.. _images_ruby:

=====================================
Images **Ruby**
=====================================


.. seealso::

   - https://store.docker.com/images/ruby
   - https://en.wikipedia.org/wiki/Ruby_%28programming_language%29




.. figure:: ruby_logo.png
   :align: center

   Le logo Ruby


Short Description
==================

Ruby is a dynamic, reflective, object-oriented, general-purpose,
open-source programming language.


What is Ruby ?
================

.. seealso::

   - https://en.wikipedia.org/wiki/Ruby_%28programming_language%29

Ruby is a dynamic, reflective, object-oriented, general-purpose,
open-source programming language.

According to its authors, Ruby was influenced by Perl, Smalltalk, Eiffel,
Ada, and Lisp.
It supports multiple programming paradigms, including functional,
object-oriented, and imperative.
It also has a dynamic type system and automatic memory management.
