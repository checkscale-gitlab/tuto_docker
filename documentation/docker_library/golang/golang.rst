

.. index::
   pair: Images ; Golang


.. _images_golang:

=====================================
Images **Go (Golang)**
=====================================


.. seealso::

   - https://store.docker.com/images/golang
   - https://en.wikipedia.org/wiki/Go_%28programming_language%29




.. figure:: golang_logo.png
   :align: center

   Le logo Golang


Short Description
==================

Node.js is a JavaScript-based platform for server-side and networking
applications.


What is Go ?
===================

.. seealso::

   - https://en.wikipedia.org/wiki/Go_%28programming_language%29

Go (a.k.a., Golang) is a programming language first developed at Google.

It is a statically-typed language with syntax loosely derived from C,
but with additional features such as garbage collection, type safety,
some dynamic-typing capabilities, additional built-in types
(e.g., variable-length arrays and key-value maps), and a large
standard library.
