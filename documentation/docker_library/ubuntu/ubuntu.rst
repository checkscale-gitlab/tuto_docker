

.. index::
   pair: Image ; Ubuntu
   pair: Ubuntu ; Xenial Xerus
   pair: Ubuntu ; Bionic
   pair: Ubuntu ; Cosmic
   pair: Ubuntu ; Disco
   ! Ubuntu


.. _images_ubuntu:

==============================
Images **Ubuntu**
==============================


.. seealso::

   - https://store.docker.com/images/ubuntu
   - https://hub.docker.com/_/ubuntu/
   - https://fr.wikipedia.org/wiki/Ubuntu_(syst%C3%A8me_d%27exploitation)





.. figure:: ubuntu-logo.png
   :align: center

   Le logo Ubuntu


.. figure:: images_ubuntu.png
   :align: center

   https://hub.docker.com/_/ubuntu/

Short Description
==================

Ubuntu is a Debian-based Linux operating system based on free software.


Description
============

.. seealso::

   - https://fr.wikipedia.org/wiki/Ubuntu_(syst%C3%A8me_d%27exploitation)

Ubuntu (prononciation : /u.bun.tu/) est un système d’exploitation
GNU/Linux basé sur la distribution Linux Debian.
Il est développé, commercialisé et maintenu pour les ordinateurs
individuels par la société Canonical.

Ubuntu se définit comme « un système d'exploitation utilisé par des
millions de PC à travers le monde »10 et avec une interface « simple, intuitive, et sécurisée ».

Elle est la distribution la plus consultée sur Internet d'après le site
Alexa. Et est le système d'exploitation le plus utilisé sur les systèmes
Cloud ainsi que sur les serveurs informatiques.

Ubuntu se divise en deux branches :

- La branche principale stable dit LTS. Avec mise à niveau tous les
  six mois et mise à jour majeure tous les 2 ans.
  La dernière version 16.04.3, nom de code *Xenial Xerus* est sortie
  le 3 août 2017.
- La branche secondaire instable avec mise à jour majeure tous les six mois.


La Philosophie d'Ubuntu
=========================

Le mot ubuntu provient d’un ancien mot bantou (famille de langues africaines)
qui désigne une personne qui prend conscience que son *moi* est
intimement lié à ce que sont les autres.
Autrement dit : **Je suis ce que je suis grâce à ce que nous sommes tous**.

C'est un concept fondamental de la « philosophie de la réconciliation »
développée par Desmond Mpilo Tutu avec l'abolition de l'apartheid.

Ubuntu signifie par ailleurs en kinyarwanda (langue rwandaise) et en
kirundi (langue burundaise) *humanité*, *générosité* ou *gratuité*.

On dit d'une chose qu'elle est *k'ubuntu* si elle est obtenue gratuitement.

En informatique, on considère qu'une distribution existe aux travers
des apports des différentes communautés Linux. Et tel qu'il se trouve
expliqué dans le travail de la Commission de la vérité et de la
réconciliation. Elles permettent de mieux saisir par exemple la mission
de la Fondation Shuttleworth relayée en France par les travaux de
philosophes comme Barbara Cassin et Philippe-Joseph Salazar.


Ubuntu Versions
=================

Disco, 19:04
----------------

.. seealso::

   - https://github.com/tianon/docker-brew-ubuntu-core/blob/1cc295b1507b68a66942b2ff5c2dbf395850208a/disco/Dockerfile


::

    FROM scratch
    ADD ubuntu-disco-core-cloudimg-amd64-root.tar.gz /

    # a few minor docker-specific tweaks
    # see https://github.com/docker/docker/blob/9a9fc01af8fb5d98b8eec0740716226fadb3735c/contrib/mkimage/debootstrap
    RUN set -xe \
        \
    # https://github.com/docker/docker/blob/9a9fc01af8fb5d98b8eec0740716226fadb3735c/contrib/mkimage/debootstrap#L40-L48
        && echo '#!/bin/sh' > /usr/sbin/policy-rc.d \
        && echo 'exit 101' >> /usr/sbin/policy-rc.d \
        && chmod +x /usr/sbin/policy-rc.d \
        \
    # https://github.com/docker/docker/blob/9a9fc01af8fb5d98b8eec0740716226fadb3735c/contrib/mkimage/debootstrap#L54-L56
        && dpkg-divert --local --rename --add /sbin/initctl \
        && cp -a /usr/sbin/policy-rc.d /sbin/initctl \
        && sed -i 's/^exit.*/exit 0/' /sbin/initctl \
        \
    # https://github.com/docker/docker/blob/9a9fc01af8fb5d98b8eec0740716226fadb3735c/contrib/mkimage/debootstrap#L71-L78
        && echo 'force-unsafe-io' > /etc/dpkg/dpkg.cfg.d/docker-apt-speedup \
        \
    # https://github.com/docker/docker/blob/9a9fc01af8fb5d98b8eec0740716226fadb3735c/contrib/mkimage/debootstrap#L85-L105
        && echo 'DPkg::Post-Invoke { "rm -f /var/cache/apt/archives/*.deb /var/cache/apt/archives/partial/*.deb /var/cache/apt/*.bin || true"; };' > /etc/apt/apt.conf.d/docker-clean \
        && echo 'APT::Update::Post-Invoke { "rm -f /var/cache/apt/archives/*.deb /var/cache/apt/archives/partial/*.deb /var/cache/apt/*.bin || true"; };' >> /etc/apt/apt.conf.d/docker-clean \
        && echo 'Dir::Cache::pkgcache ""; Dir::Cache::srcpkgcache "";' >> /etc/apt/apt.conf.d/docker-clean \
        \
    # https://github.com/docker/docker/blob/9a9fc01af8fb5d98b8eec0740716226fadb3735c/contrib/mkimage/debootstrap#L109-L115
        && echo 'Acquire::Languages "none";' > /etc/apt/apt.conf.d/docker-no-languages \
        \
    # https://github.com/docker/docker/blob/9a9fc01af8fb5d98b8eec0740716226fadb3735c/contrib/mkimage/debootstrap#L118-L130
        && echo 'Acquire::GzipIndexes "true"; Acquire::CompressionTypes::Order:: "gz";' > /etc/apt/apt.conf.d/docker-gzip-indexes \
        \
    # https://github.com/docker/docker/blob/9a9fc01af8fb5d98b8eec0740716226fadb3735c/contrib/mkimage/debootstrap#L134-L151
        && echo 'Apt::AutoRemove::SuggestsImportant "false";' > /etc/apt/apt.conf.d/docker-autoremove-suggests

    # delete all the apt list files since they're big and get stale quickly
    RUN rm -rf /var/lib/apt/lists/*
    # this forces "apt-get update" in dependent images, which is also good
    # (see also https://bugs.launchpad.net/cloud-images/+bug/1699913)

    # make systemd-detect-virt return "docker"
    # See: https://github.com/systemd/systemd/blob/aa0c34279ee40bce2f9681b496922dedbadfca19/src/basic/virt.c#L434
    RUN mkdir -p /run/systemd && echo 'docker' > /run/systemd/container

    # overwrite this with 'CMD []' in a dependent Dockerfile
    CMD ["/bin/bash"]


Cosmic, 18:10
----------------

.. seealso::

   - https://github.com/tianon/docker-brew-ubuntu-core/blob/1cc295b1507b68a66942b2ff5c2dbf395850208a/cosmic/Dockerfile

::

    FROM scratch
    ADD ubuntu-cosmic-core-cloudimg-amd64-root.tar.gz /

    # a few minor docker-specific tweaks
    # see https://github.com/docker/docker/blob/9a9fc01af8fb5d98b8eec0740716226fadb3735c/contrib/mkimage/debootstrap
    RUN set -xe \
        \
    # https://github.com/docker/docker/blob/9a9fc01af8fb5d98b8eec0740716226fadb3735c/contrib/mkimage/debootstrap#L40-L48
        && echo '#!/bin/sh' > /usr/sbin/policy-rc.d \
        && echo 'exit 101' >> /usr/sbin/policy-rc.d \
        && chmod +x /usr/sbin/policy-rc.d \
        \
    # https://github.com/docker/docker/blob/9a9fc01af8fb5d98b8eec0740716226fadb3735c/contrib/mkimage/debootstrap#L54-L56
        && dpkg-divert --local --rename --add /sbin/initctl \
        && cp -a /usr/sbin/policy-rc.d /sbin/initctl \
        && sed -i 's/^exit.*/exit 0/' /sbin/initctl \
        \
    # https://github.com/docker/docker/blob/9a9fc01af8fb5d98b8eec0740716226fadb3735c/contrib/mkimage/debootstrap#L71-L78
        && echo 'force-unsafe-io' > /etc/dpkg/dpkg.cfg.d/docker-apt-speedup \
        \
    # https://github.com/docker/docker/blob/9a9fc01af8fb5d98b8eec0740716226fadb3735c/contrib/mkimage/debootstrap#L85-L105
        && echo 'DPkg::Post-Invoke { "rm -f /var/cache/apt/archives/*.deb /var/cache/apt/archives/partial/*.deb /var/cache/apt/*.bin || true"; };' > /etc/apt/apt.conf.d/docker-clean \
        && echo 'APT::Update::Post-Invoke { "rm -f /var/cache/apt/archives/*.deb /var/cache/apt/archives/partial/*.deb /var/cache/apt/*.bin || true"; };' >> /etc/apt/apt.conf.d/docker-clean \
        && echo 'Dir::Cache::pkgcache ""; Dir::Cache::srcpkgcache "";' >> /etc/apt/apt.conf.d/docker-clean \
        \
    # https://github.com/docker/docker/blob/9a9fc01af8fb5d98b8eec0740716226fadb3735c/contrib/mkimage/debootstrap#L109-L115
        && echo 'Acquire::Languages "none";' > /etc/apt/apt.conf.d/docker-no-languages \
        \
    # https://github.com/docker/docker/blob/9a9fc01af8fb5d98b8eec0740716226fadb3735c/contrib/mkimage/debootstrap#L118-L130
        && echo 'Acquire::GzipIndexes "true"; Acquire::CompressionTypes::Order:: "gz";' > /etc/apt/apt.conf.d/docker-gzip-indexes \
        \
    # https://github.com/docker/docker/blob/9a9fc01af8fb5d98b8eec0740716226fadb3735c/contrib/mkimage/debootstrap#L134-L151
        && echo 'Apt::AutoRemove::SuggestsImportant "false";' > /etc/apt/apt.conf.d/docker-autoremove-suggests

    # delete all the apt list files since they're big and get stale quickly
    RUN rm -rf /var/lib/apt/lists/*
    # this forces "apt-get update" in dependent images, which is also good
    # (see also https://bugs.launchpad.net/cloud-images/+bug/1699913)

    # make systemd-detect-virt return "docker"
    # See: https://github.com/systemd/systemd/blob/aa0c34279ee40bce2f9681b496922dedbadfca19/src/basic/virt.c#L434
    RUN mkdir -p /run/systemd && echo 'docker' > /run/systemd/container

    # overwrite this with 'CMD []' in a dependent Dockerfile
    CMD ["/bin/bash"]



Bionic, 18:04
----------------

.. seealso::

   - https://github.com/tianon/docker-brew-ubuntu-core/blob/1cc295b1507b68a66942b2ff5c2dbf395850208a/bionic/Dockerfile


::


    FROM scratch
    ADD ubuntu-bionic-core-cloudimg-amd64-root.tar.gz /

    # a few minor docker-specific tweaks
    # see https://github.com/docker/docker/blob/9a9fc01af8fb5d98b8eec0740716226fadb3735c/contrib/mkimage/debootstrap
    RUN set -xe \
        \
    # https://github.com/docker/docker/blob/9a9fc01af8fb5d98b8eec0740716226fadb3735c/contrib/mkimage/debootstrap#L40-L48
        && echo '#!/bin/sh' > /usr/sbin/policy-rc.d \
        && echo 'exit 101' >> /usr/sbin/policy-rc.d \
        && chmod +x /usr/sbin/policy-rc.d \
        \
    # https://github.com/docker/docker/blob/9a9fc01af8fb5d98b8eec0740716226fadb3735c/contrib/mkimage/debootstrap#L54-L56
        && dpkg-divert --local --rename --add /sbin/initctl \
        && cp -a /usr/sbin/policy-rc.d /sbin/initctl \
        && sed -i 's/^exit.*/exit 0/' /sbin/initctl \
        \
    # https://github.com/docker/docker/blob/9a9fc01af8fb5d98b8eec0740716226fadb3735c/contrib/mkimage/debootstrap#L71-L78
        && echo 'force-unsafe-io' > /etc/dpkg/dpkg.cfg.d/docker-apt-speedup \
        \
    # https://github.com/docker/docker/blob/9a9fc01af8fb5d98b8eec0740716226fadb3735c/contrib/mkimage/debootstrap#L85-L105
        && echo 'DPkg::Post-Invoke { "rm -f /var/cache/apt/archives/*.deb /var/cache/apt/archives/partial/*.deb /var/cache/apt/*.bin || true"; };' > /etc/apt/apt.conf.d/docker-clean \
        && echo 'APT::Update::Post-Invoke { "rm -f /var/cache/apt/archives/*.deb /var/cache/apt/archives/partial/*.deb /var/cache/apt/*.bin || true"; };' >> /etc/apt/apt.conf.d/docker-clean \
        && echo 'Dir::Cache::pkgcache ""; Dir::Cache::srcpkgcache "";' >> /etc/apt/apt.conf.d/docker-clean \
        \
    # https://github.com/docker/docker/blob/9a9fc01af8fb5d98b8eec0740716226fadb3735c/contrib/mkimage/debootstrap#L109-L115
        && echo 'Acquire::Languages "none";' > /etc/apt/apt.conf.d/docker-no-languages \
        \
    # https://github.com/docker/docker/blob/9a9fc01af8fb5d98b8eec0740716226fadb3735c/contrib/mkimage/debootstrap#L118-L130
        && echo 'Acquire::GzipIndexes "true"; Acquire::CompressionTypes::Order:: "gz";' > /etc/apt/apt.conf.d/docker-gzip-indexes \
        \
    # https://github.com/docker/docker/blob/9a9fc01af8fb5d98b8eec0740716226fadb3735c/contrib/mkimage/debootstrap#L134-L151
        && echo 'Apt::AutoRemove::SuggestsImportant "false";' > /etc/apt/apt.conf.d/docker-autoremove-suggests

    # delete all the apt list files since they're big and get stale quickly
    RUN rm -rf /var/lib/apt/lists/*
    # this forces "apt-get update" in dependent images, which is also good
    # (see also https://bugs.launchpad.net/cloud-images/+bug/1699913)

    # make systemd-detect-virt return "docker"
    # See: https://github.com/systemd/systemd/blob/aa0c34279ee40bce2f9681b496922dedbadfca19/src/basic/virt.c#L434
    RUN mkdir -p /run/systemd && echo 'docker' > /run/systemd/container

    # overwrite this with 'CMD []' in a dependent Dockerfile
    CMD ["/bin/bash"]
