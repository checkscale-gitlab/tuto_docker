.. index::
   pair: Image ; Apache HTTP Bitnami server


.. _images_apache_bitnami_httpd:

=================================
Images **Apache HTTPD** bitnami
=================================

.. seealso::

   - https://twitter.com/Bitnami
   - https://hub.docker.com/r/bitnami/apache/
   - https://github.com/bitnami/bitnami-docker-apache
   - https://en.wikipedia.org/wiki/Apache_HTTP_Server
   - https://httpd.apache.org/





Short Description
==================

What is Apache ?
-------------------

The Apache HTTP Server Project is an effort to develop and maintain an
open-source HTTP server for modern operating systems including UNIX
and Windows NT.

The goal of this project is to provide a secure, efficient and extensible
server that provides HTTP services in sync with the current HTTP standards.

TL;DR;
========

::

    $ docker run --name apache bitnami/apache:latest


Docker Compose
==================

::

    $ curl -sSL https://raw.githubusercontent.com/bitnami/bitnami-docker-apache/master/docker-compose.yml > docker-compose.yml

::

    $ docker-compose up -d


Dockerfile
============

::

    FROM bitnami/minideb-extras:stretch-r158
    LABEL maintainer "Bitnami <containers@bitnami.com>"

    ENV BITNAMI_PKG_CHMOD="-R g+rwX" \
        BITNAMI_PKG_EXTRA_DIRS="/bitnami/apache/conf /opt/bitnami/apache/tmp /opt/bitnami/apache/conf" \
        HOME="/"

    # Install required system packages and dependencies
    RUN install_packages libc6 libexpat1 libffi6 libgmp10 libgnutls30 libhogweed4 libidn11 libldap-2.4-2 libnettle6 libp11-kit0 libpcre3 libsasl2-2 libssl1.1 libtasn1-6 zlib1g
    RUN bitnami-pkg unpack apache-2.4.35-0 --checksum 1e352e2185137fcad60bb6fdf2961368f59a35e2c2cab4ee94c77152f1c37299
    RUN ln -sf /opt/bitnami/apache/htdocs /app
    RUN ln -sf /dev/stdout /opt/bitnami/apache/logs/access_log
    RUN ln -sf /dev/stdout /opt/bitnami/apache/logs/error_log

    COPY rootfs /
    ENV APACHE_HTTPS_PORT_NUMBER="8443" \
        APACHE_HTTP_PORT_NUMBER="8080" \
        BITNAMI_APP_NAME="apache" \
        BITNAMI_IMAGE_VERSION="2.4.35-debian-9-r10" \
        PATH="/opt/bitnami/apache/bin:$PATH"

    EXPOSE 8080 8443

    WORKDIR /app
    USER 1001
    ENTRYPOINT [ "/app-entrypoint.sh" ]
    CMD [ "/run.sh" ]


Why use Bitnami Images ?
===========================

Bitnami closely tracks upstream source changes and promptly publishes
new versions of this image using our automated systems.

With Bitnami images the latest bug fixes and features are available
as soon as possible.

Bitnami containers, virtual machines and cloud images use the same
components and configuration approach - making it easy to switch
between formats based on your project needs.

Bitnami images are built on CircleCI and automatically pushed to the
Docker Hub.

All our images are based on minideb a minimalist Debian based container
image which gives you a small base container image and the familiarity
of a leading linux distribution.


Adding custom virtual hosts
===============================

The default httpd.conf includes virtual hosts placed in /bitnami/apache/conf/vhosts/.

You can mount a my_vhost.conf file containing your custom virtual hosts
at this location.

Step 1: Write your my_vhost.conf file with the following content
-------------------------------------------------------------------


.. code-block:: apache

    <VirtualHost *:8080>
      ServerName www.example.com
      DocumentRoot "/app"
      <Directory "/app">
        Options Indexes FollowSymLinks
        AllowOverride All
        Require all granted
      </Directory>
    </VirtualHost>

Step 2: Mount the configuration as a volume
-----------------------------------------------

::

    $ docker run --name apache \
      -v /path/to/my_vhost.conf:/bitnami/apache/conf/vhosts/my_vhost.conf:ro \
      bitnami/apache:latest

or using Docker Compose::

    version: '2'

    services:
      apache:
        image: 'bitnami/apache:latest'
        ports:
          - '80:8080'
          - '443:8443'
        volumes:
          - /path/to/my_vhost.conf:/bitnami/apache/conf/vhosts/my_vhost.conf:ro



Using custom SSL certificates
=================================

.. note:: The steps below assume that you are using a custom domain name
  and that you have already configured the custom domain name to point
  to your server.

This container comes with SSL support already pre-configured and with a
dummy certificate in place (server.crt and server.key files in
/bitnami/apache/conf/bitnami/certs).

If you want to use your own certificate (.crt) and certificate key (.key)
files, follow the steps below:

Step 1: Prepare your certificate files
----------------------------------------


In your local computer, create a folder called certs and put your
certificates files. Make sure you rename both files to server.crt and server.key respectively:

::

    $ mkdir /path/to/apache-persistence/apache/conf/bitnami/certs -p
    $ cp /path/to/certfile.crt /path/to/apache-persistence/apache/conf/bitnami/certs
    cp /path/to/keyfile.key  /path/to/apache-persistence/apache/conf/bitnami/certs/server.key


Step 2: Run the Apache image
-------------------------------

Run the Apache image, mounting the certificates directory from your host.

::

    $ docker run --name apache \
      -v /path/to/apache-persistence/apache/conf/bitnami/certs:/bitnami/apache/conf/bitnami/certs \
      bitnami/apache:latest



or using Docker Compose::

    version: '2'

    services:
      apache:
        image: 'bitnami/apache:latest'
        ports:
          - '80:8080'
          - '443:8443'
        volumes:
          - /path/to/apache-persistence/apache/conf/bitnami/certs:/bitnami/apache/conf/bitnami/certs


Full configuration
====================

The image looks for configurations in /bitnami/apache/conf/.

You can mount a volume at /bitnami and copy/edit the configurations in
the /bitnami/apache/conf/.
The default configurations will be populated in the conf/ directory if
it's empty.


Step 1: Run the Apache image
-------------------------------

Run the Apache image, mounting a directory from your host.

::

    $ docker run --name apache \
      -v /path/to/apache-persistence:/bitnami \
      bitnami/apache:latest

or using Docker Compose::

    version: '2'

        services:
          apache:
            image: 'bitnami/apache:latest'
            ports:
              - '80:8080'
              - '443:8443'
            volumes:
              - /path/to/apache-persistence:/bitnami


Step 2: Edit the configuration
--------------------------------

Edit the configuration on your host using your favorite editor.

::

    $ vi /path/to/apache-persistence/apache/conf/httpd.conf


Step 3: Restart Apache
-------------------------

After changing the configuration, restart your Apache container for the
changes to take effect.

::

    $ docker restart apache

or using Docker Compose::

    $ docker-compose restart apache


Logging
=========

The Bitnami Apache Docker image sends the container logs to the stdout.
To view the logs::


    $ docker logs apache

or using Docker Compose::

    $ docker-compose logs apache

You can configure the containers logging driver using the --log-driver
option if you wish to consume the container logs differently.

In the default configuration docker uses the json-file driver.


Upgrade this image
=====================

Bitnami provides up-to-date versions of Apache, including security patches,
soon after they are made upstream. We recommend that you follow these
steps to upgrade your container.

Step 1: Get the updated image
---------------------------------

::

    $ docker pull bitnami/apache:latest

::

    or if you're using Docker Compose, update the value of the image
    property to bitnami/apache:latest.


Step 2: Stop and backup the currently running container
-----------------------------------------------------------

Stop the currently running container using the command

::

    $ docker stop apache


or using Docker Compose::

    $ docker-compose stop apache

Next, take a snapshot of the persistent volume /path/to/apache-persistence using:

::

    $ rsync -a /path/to/apache-persistence /path/to/apache-persistence.bkp.$(date +%Y%m%d-%H.%M.%S)


You can use this snapshot to restore the database state should the
upgrade fail ??.


Step 3: Remove the currently running container
------------------------------------------------

::

    $ docker rm -v apache


or using Docker Compose:


    $ docker-compose rm -v apache


Step 4: Run the new image
----------------------------

Re-create your container from the new image.

::

    $ docker run --name apache bitnami/apache:latest

or using Docker Compose:

    $ docker-compose up apache


Notable Changes
================

2.4.34-r8 (2018-07-24)
-------------------------

.. seealso::

   - https://github.com/bitnami/bitnami-docker-apache/tree/2.4.34-ol-7-r8


The Apache container has been migrated to a non-root user approach.

Previously the container ran as the root user and the Apache daemon was
started as the apache user.

From now on, both the container and the Apache daemon run as user 1001.

As a consequence, the HTTP/HTTPS ports exposed by the container are now
**8080/8443 instead of 80/443**

You can revert this behavior by changing USER 1001 to USER root in
the Dockerfile.

2.4.18-r0
-----------

The configuration volume has been moved to /bitnami/apache.

Now you only need to mount a single volume at /bitnami/apache for
persisting configuration.

/app is still used for serving content by the default virtual host.

The logs are always sent to the stdout and are no longer collected in
the volume.

2.4.12-4-r01
-----------------


The /app directory is no longer exported as a volume.

This caused problems when building on top of the image, since changes
in the volume are not persisted between Dockerfile RUN instructions.

To keep the previous behavior (so that you can mount the volume in
another container), create the container with the -v /app option.
