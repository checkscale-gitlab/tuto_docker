

.. index::
   pair: Images ; wordpress


.. _images_wordpress:

=====================================
Images **Wordpress**
=====================================


.. seealso::

   - https://store.docker.com/images/wordpress




.. figure:: wordpress_logo.png
   :align: center

   Le logo redmine


Short Description
==================

The WordPress rich content management system can utilize plugins, widgets,
and themes.
