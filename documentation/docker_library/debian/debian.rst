

.. index::
   pair: Image ; Debian
   pair: Buster ; Debian
   ! Debian


.. _images_debian:

==============================
Images **Debian**
==============================


.. seealso::

   - https://store.docker.com/images/debian
   - https://hub.docker.com/_/debian/
   - https://fr.wikipedia.org/wiki/Debian






.. figure:: Debian-OpenLogo.svg.png
   :align: center

   Logo Debian



.. figure:: images_debian.png
   :align: center

   https://hub.docker.com/_/debian/

Short Description
==================

Debian is a Linux distribution that's composed entirely of free and
open-source software.


Description
============

.. seealso::

   - https://fr.wikipedia.org/wiki/Debian

Debian (/de.bjan/) est une organisation communautaire et démocratique,
dont le but est le développement de systèmes d'exploitation basés
exclusivement sur des logiciels libres.

Chaque système, lui-même nommé Debian, réunit autour d'un noyau de
système d'exploitation de nombreux éléments pouvant être développés
indépendamment les uns des autres, pour plusieurs architectures
matérielles. Ces éléments, programmes de base complétant le noyau et
logiciels applicatifs, se présentent sous forme de « paquets » qui
peuvent être installés en fonction des besoins (voir Distribution des
logiciels). L'ensemble système d'exploitation plus logiciels s'appelle
une distribution.

On assimile généralement ces systèmes d'exploitation au système
Debian GNU/Linux, la distribution GNU/Linux de Debian, car jusqu'en 2009
c'était la seule branche parfaitement fonctionnelle. Mais d'autres
distributions Debian sont en cours de développement en 2013 :
Debian GNU/Hurd3, et Debian GNU/kFreeBSD5. La version Debian
*Squeeze* est la première à être distribuée avec le noyau
kFreeBSD en plus du noyau Linux6.

Debian est utilisée comme base de nombreuses autres distributions
telles que Knoppix et Ubuntu qui rencontrent un grand succès.


Versions
===========

buster
--------


.. seealso::

   - https://github.com/debuerreotype/docker-debian-artifacts/blob/a5e61a4c40a4b366d614715c51f883e0b153afb5/buster/Dockerfile


::

    FROM scratch
    ADD rootfs.tar.xz /
    CMD ["bash"]
