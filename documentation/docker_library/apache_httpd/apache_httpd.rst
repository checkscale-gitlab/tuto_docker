

.. index::
   pair: Images ; Apache HTTP server
   pair: 2.4 ; Apache HTTP server


.. _images_apache_httpd:

==============================
Images **Apache HTTPD**
==============================


.. seealso::

   - https://hub.docker.com/_/httpd/
   - https://en.wikipedia.org/wiki/Apache_HTTP_Server
   - https://httpd.apache.org/





.. figure:: apache_httpd_logo.png
   :align: center

   Le logo Apache HTTPD


.. figure:: images_apache_httpd.png
   :align: center

   https://hub.docker.com/_/httpd/


Short Description
==================

The Apache HTTP Server Project.


What is httpd ?
=================

The Apache HTTP Server, colloquially called **Apache**, is a Web server
application notable for playing a key role in the initial growth of the
World Wide Web.

Originally based on the NCSA HTTPd server, development of Apache began
in early 1995 after work on the NCSA code stalled.

Apache quickly overtook NCSA HTTPd as the dominant HTTP server, and has
remained the most popular HTTP server in use since April 1996.


Configuration
================

To customize the configuration of the httpd server, just COPY your
custom configuration in as /usr/local/apache2/conf/httpd.conf.

::

    FROM httpd:2.4
    COPY ./my-httpd.conf /usr/local/apache2/conf/httpd.conf


SSL/HTTPS
============

If you want to run your web traffic over SSL, the simplest setup is to
COPY or mount (-v) your server.crt and server.key into /usr/local/apache2/conf/
and then customize the /usr/local/apache2/conf/httpd.conf by removing
the comment symbol from the following lines::

    ...
    #LoadModule socache_shmcb_module modules/mod_socache_shmcb.so
    ...
    #LoadModule ssl_module modules/mod_ssl.so
    ...
    #Include conf/extra/httpd-ssl.conf
    ...


The conf/extra/httpd-ssl.conf configuration file will use the certificate
files previously added and tell the daemon to also listen on port 443.

Be sure to also add something like -p 443:443 to your docker run to
forward the https port.

This could be accomplished with a sed line similar to the following::

    RUN sed -i \
            -e 's/^#\(Include .*httpd-ssl.conf\)/\1/' \
            -e 's/^#\(LoadModule .*mod_ssl.so\)/\1/' \
            -e 's/^#\(LoadModule .*mod_socache_shmcb.so\)/\1/' \
            conf/httpd.conf

The previous steps should work well for development, but we recommend
customizing your conf files for production, see httpd.apache.org for more
information about SSL setup.



Versions
==========

.. seealso::

   - https://docs.docker.com/samples/library/httpd/


2.4 classic
-------------

.. seealso::

   - https://github.com/docker-library/httpd/blob/5a6a1d99f1d6e754ecfcdd7a13e12980b86d7b75/2.4/Dockerfile


::

    FROM debian:stretch-slim

    # add our user and group first to make sure their IDs get assigned consistently, regardless of whatever dependencies get added
    #RUN groupadd -r www-data && useradd -r --create-home -g www-data www-data

    ENV HTTPD_PREFIX /usr/local/apache2
    ENV PATH $HTTPD_PREFIX/bin:$PATH
    RUN mkdir -p "$HTTPD_PREFIX" \
        && chown www-data:www-data "$HTTPD_PREFIX"
    WORKDIR $HTTPD_PREFIX

    # install httpd runtime dependencies
    # https://httpd.apache.org/docs/2.4/install.html#requirements
    RUN set -eux; \
        apt-get update; \
        apt-get install -y --no-install-recommends \
            libapr1-dev \
            libaprutil1-dev \
            libaprutil1-ldap \
        ; \
        rm -rf /var/lib/apt/lists/*

    ENV HTTPD_VERSION 2.4.38
    ENV HTTPD_SHA256 7dc65857a994c98370dc4334b260101a7a04be60e6e74a5c57a6dee1bc8f394a

    # https://httpd.apache.org/security/vulnerabilities_24.html
    ENV HTTPD_PATCHES=""

    ENV APACHE_DIST_URLS \
    # https://issues.apache.org/jira/browse/INFRA-8753?focusedCommentId=14735394#comment-14735394
        https://www.apache.org/dyn/closer.cgi?action=download&filename= \
    # if the version is outdated (or we're grabbing the .asc file), we might have to pull from the dist/archive :/
        https://www-us.apache.org/dist/ \
        https://www.apache.org/dist/ \
        https://archive.apache.org/dist/

    # see https://httpd.apache.org/docs/2.4/install.html#requirements
    RUN set -eux; \
        \
        # mod_http2 mod_lua mod_proxy_html mod_xml2enc
        # https://anonscm.debian.org/cgit/pkg-apache/apache2.git/tree/debian/control?id=adb6f181257af28ee67af15fc49d2699a0080d4c
        savedAptMark="$(apt-mark showmanual)"; \
        apt-get update; \
        apt-get install -y --no-install-recommends \
            bzip2 \
            ca-certificates \
            dirmngr \
            dpkg-dev \
            gcc \
            gnupg \
            liblua5.2-dev \
            libnghttp2-dev \
            libpcre3-dev \
            libssl-dev \
            libxml2-dev \
            make \
            wget \
            zlib1g-dev \
        ; \
        rm -r /var/lib/apt/lists/*; \
        \
        ddist() { \
            local f="$1"; shift; \
            local distFile="$1"; shift; \
            local success=; \
            local distUrl=; \
            for distUrl in $APACHE_DIST_URLS; do \
                if wget -O "$f" "$distUrl$distFile" && [ -s "$f" ]; then \
                    success=1; \
                    break; \
                fi; \
            done; \
            [ -n "$success" ]; \
        }; \
        \
        ddist 'httpd.tar.bz2' "httpd/httpd-$HTTPD_VERSION.tar.bz2"; \
        echo "$HTTPD_SHA256 *httpd.tar.bz2" | sha256sum -c -; \
        \
    # see https://httpd.apache.org/download.cgi#verify
        ddist 'httpd.tar.bz2.asc' "httpd/httpd-$HTTPD_VERSION.tar.bz2.asc"; \
        export GNUPGHOME="$(mktemp -d)"; \
        for key in \
    # gpg: key 791485A8: public key "Jim Jagielski (Release Signing Key) <jim@apache.org>" imported
            A93D62ECC3C8EA12DB220EC934EA76E6791485A8 \
    # gpg: key 995E35221AD84DFF: public key "Daniel Ruggeri (http://home.apache.org/~druggeri/) <druggeri@apache.org>" imported
            B9E8213AEFB861AF35A41F2C995E35221AD84DFF \
        ; do \
            gpg --batch --keyserver ha.pool.sks-keyservers.net --recv-keys "$key"; \
        done; \
        gpg --batch --verify httpd.tar.bz2.asc httpd.tar.bz2; \
        command -v gpgconf && gpgconf --kill all || :; \
        rm -rf "$GNUPGHOME" httpd.tar.bz2.asc; \
        \
        mkdir -p src; \
        tar -xf httpd.tar.bz2 -C src --strip-components=1; \
        rm httpd.tar.bz2; \
        cd src; \
        \
        patches() { \
            while [ "$#" -gt 0 ]; do \
                local patchFile="$1"; shift; \
                local patchSha256="$1"; shift; \
                ddist "$patchFile" "httpd/patches/apply_to_$HTTPD_VERSION/$patchFile"; \
                echo "$patchSha256 *$patchFile" | sha256sum -c -; \
                patch -p0 < "$patchFile"; \
                rm -f "$patchFile"; \
            done; \
        }; \
        patches $HTTPD_PATCHES; \
        \
        gnuArch="$(dpkg-architecture --query DEB_BUILD_GNU_TYPE)"; \
        ./configure \
            --build="$gnuArch" \
            --prefix="$HTTPD_PREFIX" \
            --enable-mods-shared=reallyall \
            --enable-mpms-shared=all \
        ; \
        make -j "$(nproc)"; \
        make install; \
        \
        cd ..; \
        rm -r src man manual; \
        \
        sed -ri \
            -e 's!^(\s*CustomLog)\s+\S+!\1 /proc/self/fd/1!g' \
            -e 's!^(\s*ErrorLog)\s+\S+!\1 /proc/self/fd/2!g' \
            "$HTTPD_PREFIX/conf/httpd.conf"; \
        \
    # reset apt-mark's "manual" list so that "purge --auto-remove" will remove all build dependencies
        apt-mark auto '.*' > /dev/null; \
        [ -z "$savedAptMark" ] || apt-mark manual $savedAptMark; \
        find /usr/local -type f -executable -exec ldd '{}' ';' \
            | awk '/=>/ { print $(NF-1) }' \
            | sort -u \
            | xargs -r dpkg-query --search \
            | cut -d: -f1 \
            | sort -u \
            | xargs -r apt-mark manual \
        ; \
        apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false; \
        \
    # smoke test
        httpd -v

    COPY httpd-foreground /usr/local/bin/

    EXPOSE 80
    CMD ["httpd-foreground"]



2.4 Alpine
-------------

::

    FROM alpine:3.9

    # ensure www-data user exists
    RUN set -x \
        && addgroup -g 82 -S www-data \
        && adduser -u 82 -D -S -G www-data www-data
    # 82 is the standard uid/gid for "www-data" in Alpine
    # https://git.alpinelinux.org/cgit/aports/tree/main/apache2/apache2.pre-install?h=v3.8.1
    # https://git.alpinelinux.org/cgit/aports/tree/main/lighttpd/lighttpd.pre-install?h=v3.8.1
    # https://git.alpinelinux.org/cgit/aports/tree/main/nginx/nginx.pre-install?h=v3.8.1

    ENV HTTPD_PREFIX /usr/local/apache2
    ENV PATH $HTTPD_PREFIX/bin:$PATH
    RUN mkdir -p "$HTTPD_PREFIX" \
        && chown www-data:www-data "$HTTPD_PREFIX"
    WORKDIR $HTTPD_PREFIX

    ENV HTTPD_VERSION 2.4.38
    ENV HTTPD_SHA256 7dc65857a994c98370dc4334b260101a7a04be60e6e74a5c57a6dee1bc8f394a

    # https://httpd.apache.org/security/vulnerabilities_24.html
    ENV HTTPD_PATCHES=""

    ENV APACHE_DIST_URLS \
    # https://issues.apache.org/jira/browse/INFRA-8753?focusedCommentId=14735394#comment-14735394
        https://www.apache.org/dyn/closer.cgi?action=download&filename= \
    # if the version is outdated (or we're grabbing the .asc file), we might have to pull from the dist/archive :/
        https://www-us.apache.org/dist/ \
        https://www.apache.org/dist/ \
        https://archive.apache.org/dist/

    # see https://httpd.apache.org/docs/2.4/install.html#requirements
    RUN set -eux; \
        \
        runDeps=' \
            apr-dev \
            apr-util-dev \
            apr-util-ldap \
            perl \
        '; \
        apk add --no-cache --virtual .build-deps \
            $runDeps \
            ca-certificates \
            coreutils \
            dpkg-dev dpkg \
            gcc \
            gnupg \
            libc-dev \
            # mod_proxy_html mod_xml2enc
            libxml2-dev \
            # mod_lua
            lua-dev \
            make \
            # mod_http2
            nghttp2-dev \
            # mod_session_crypto
            openssl \
            openssl-dev \
            pcre-dev \
            tar \
            # mod_deflate
            zlib-dev \
        ; \
        \
        ddist() { \
            local f="$1"; shift; \
            local distFile="$1"; shift; \
            local success=; \
            local distUrl=; \
            for distUrl in $APACHE_DIST_URLS; do \
                if wget -O "$f" "$distUrl$distFile" && [ -s "$f" ]; then \
                    success=1; \
                    break; \
                fi; \
            done; \
            [ -n "$success" ]; \
        }; \
        \
        ddist 'httpd.tar.bz2' "httpd/httpd-$HTTPD_VERSION.tar.bz2"; \
        echo "$HTTPD_SHA256 *httpd.tar.bz2" | sha256sum -c -; \
        \
    # see https://httpd.apache.org/download.cgi#verify
        ddist 'httpd.tar.bz2.asc' "httpd/httpd-$HTTPD_VERSION.tar.bz2.asc"; \
        export GNUPGHOME="$(mktemp -d)"; \
        for key in \
    # gpg: key 791485A8: public key "Jim Jagielski (Release Signing Key) <jim@apache.org>" imported
            A93D62ECC3C8EA12DB220EC934EA76E6791485A8 \
    # gpg: key 995E35221AD84DFF: public key "Daniel Ruggeri (https://home.apache.org/~druggeri/) <druggeri@apache.org>" imported
            B9E8213AEFB861AF35A41F2C995E35221AD84DFF \
        ; do \
            gpg --batch --keyserver ha.pool.sks-keyservers.net --recv-keys "$key"; \
        done; \
        gpg --batch --verify httpd.tar.bz2.asc httpd.tar.bz2; \
        command -v gpgconf && gpgconf --kill all || :; \
        rm -rf "$GNUPGHOME" httpd.tar.bz2.asc; \
        \
        mkdir -p src; \
        tar -xf httpd.tar.bz2 -C src --strip-components=1; \
        rm httpd.tar.bz2; \
        cd src; \
        \
        patches() { \
            while [ "$#" -gt 0 ]; do \
                local patchFile="$1"; shift; \
                local patchSha256="$1"; shift; \
                ddist "$patchFile" "httpd/patches/apply_to_$HTTPD_VERSION/$patchFile"; \
                echo "$patchSha256 *$patchFile" | sha256sum -c -; \
                patch -p0 < "$patchFile"; \
                rm -f "$patchFile"; \
            done; \
        }; \
        patches $HTTPD_PATCHES; \
        \
        gnuArch="$(dpkg-architecture --query DEB_BUILD_GNU_TYPE)"; \
        ./configure \
            --build="$gnuArch" \
            --prefix="$HTTPD_PREFIX" \
            --enable-mods-shared=reallyall \
            --enable-mpms-shared=all \
        ; \
        make -j "$(nproc)"; \
        make install; \
        \
        cd ..; \
        rm -r src man manual; \
        \
        sed -ri \
            -e 's!^(\s*CustomLog)\s+\S+!\1 /proc/self/fd/1!g' \
            -e 's!^(\s*ErrorLog)\s+\S+!\1 /proc/self/fd/2!g' \
            "$HTTPD_PREFIX/conf/httpd.conf"; \
        \
        runDeps="$runDeps $( \
            scanelf --needed --nobanner --format '%n#p' --recursive /usr/local \
                | tr ',' '\n' \
                | sort -u \
                | awk 'system("[ -e /usr/local/lib/" $1 " ]") == 0 { next } { print "so:" $1 }' \
        )"; \
        apk add --virtual .httpd-rundeps $runDeps; \
        apk del .build-deps; \
        \
    # smoke test
        httpd -v

    COPY httpd-foreground /usr/local/bin/

    EXPOSE 80
    CMD ["httpd-foreground"]
