

.. index::
   pair: Private ; GitLab Registry


.. _private_gitlab_registry:

============================================
Private GitLab Container Registry
============================================


.. seealso::

   - https://docs.gitlab.com/ce/user/project/container_registry.html


.. contents::
   :depth: 5



Utilisation sous GNU/Linux
=============================


/etc/docker/daemon.json
--------------------------


::

    pvergain@UC004:~$ cat /etc/docker/daemon.json
    {
      "insecure-registries" : ["dockerhub.srv.int.id3.eu:5555"]
    }


docker login
--------------

::

    docker login -u gitlab-ci-token -p "XXXXXXXXX" "dockerhub.srv.int.id3.eu:5555"


restart docker
----------------


::

    sudo systemctl restart docker.service



docker tag
============

::

    docker tag be4f0 transactions_colombie:4_2_0_dev



docker push
==============

::

    docker push
