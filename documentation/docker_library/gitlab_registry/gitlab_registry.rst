

.. index::
   pair: GitLab ; Registry


.. _gitlab_registry:

============================================
GitLab Container Registry
============================================


.. seealso::

   - https://docs.gitlab.com/ce/user/project/container_registry.html


.. contents::
   :depth: 5



Introduction
================

With the Docker Container Registry integrated into GitLab, **every project
can have its own space to store its Docker images**.



Private registry
=================

.. toctree::
   :maxdepth: 3

   private/private
