

.. index::
   pair: Images ; Apache Tomcat


.. _images_apache_tomcat:

==============================
Images **apache Tomcat**
==============================


.. seealso::

   - https://hub.docker.com/_/tomcat/
   - https://fr.wikipedia.org/wiki/Apache_Tomcat




.. figure:: apache_tomcat_logo.png
   :align: center

   Le logo Apache Tomcat


.. figure:: images_tomcat.png
   :align: center

   https://hub.docker.com/_/tomcat/


Short Description
==================

Apache Tomcat is an open source implementation of the Java Servlet and
JavaServer Pages technologies


What is Apache Tomcat ?
=========================

Apache Tomcat (or simply Tomcat) is an open source web server and
servlet container developed by the Apache Software Foundation (ASF).

Tomcat implements the Java Servlet and the JavaServer Pages (JSP)
specifications from Oracle, and provides a "pure Java" HTTP web server
environment for Java code to run in.

In the simplest config Tomcat runs in a single operating system process.

The process runs a Java virtual machine (JVM).

Every single HTTP request from a browser to Tomcat is processed in the
Tomcat process in a separate thread.
