

.. index::
   pair: Images ; Anaconda3


.. _images_anaconda3:

==============================
Images **Anaconda3**
==============================


.. seealso::

   - https://docs.anaconda.com/anaconda/user-guide/tasks/integration/docker
   - https://hub.docker.com/r/continuumio/
   - https://hub.docker.com/r/continuumio/anaconda3/
   - https://github.com/ContinuumIO/docker-images
   - https://docs.anaconda.com/anaconda/glossary




.. figure:: continuumio_logo.png
   :align: center

   Le logo Continuumio


Short Description
==================

Powerful and flexible python distribution.


Usage
========

You can download and run this image using the following commands:


::

    C:\Tmp>docker pull continuumio/anaconda3

::

	Using default tag: latest
	latest: Pulling from continuumio/anaconda3
	85b1f47fba49: Pull complete
	f4070d96116d: Pull complete
	8b1142e4866d: Pull complete
	924a14505c9a: Pull complete
	Digest: sha256:c6fb10532fe2efac2f61bd4941896b917ad7b7f197bda9bddd3943aee434d281
    Status: Downloaded newer image for continuumio/anaconda3:latest


::

    C:\Tmp>docker run -i -t continuumio/anaconda3 /bin/bash

::

    root@8ffcde2f70f6:/# uname -a

::

    Linux 8ffcde2f70f6 4.9.60-linuxkit-aufs #1 SMP Mon Nov 6 16:00:12 UTC 2017 x86_64 GNU/Linux

::

    root@8ffcde2f70f6:/# which python

::

    /opt/conda/bin/python

::

    root@8ffcde2f70f6:/# python

::

	Python 3.6.3 |Anaconda, Inc.| (default, Oct 13 2017, 12:02:49)
	[GCC 7.2.0] on linux
	Type "help", "copyright", "credits" or "license" for more information.
	>>>
