

.. index::
   pair: Image ; static-site


.. _image_static_site:

======================================
Image **dockersamples/static-site**
======================================


.. seealso::

   - https://hub.docker.com/r/dockersamples/static-site/
