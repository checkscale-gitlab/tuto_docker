

.. _docker_compose_2_0_0:

=============================================
**docker-compose 2.0.0 (V2, 2021-09-21)**
=============================================

- :ref:`docker_compose_V2`
- https://github.com/docker/compose/tree/v2
- https://github.com/docker/compose/releases/tag/v2.0.0
- https://www.cloudsavvyit.com/12144/whats-new-in-docker-compose-v2/

Docker Compose V2
======================

- :ref:`docker_compose_V2`
- https://github.com/docker/compose#where-to-get-docker-compose

Docker Compose V2 is a major version bump release of Docker Compose.

It has been completely rewritten from scratch in Golang (V1 was in Python).
The installation instructions for Compose V2 differ from V1.

V2 is not a standalone binary anymore, and installation scripts will
have to be adjusted.

Some commands are different.

For a smooth transition from legacy docker-compose 1.xx, please consider
installing compose-switch to translate docker-compose ... commands into
Compose V2's docker compose .... . Also check V2's --compatibility flag.

Description
==============

Compose v2 architecture and installation instructions differ from v1,
see `README.md <https://github.com/docker/compose#where-to-get-docker-compose>`_

For backward compatibility with docker-compose v1.x, consider installing
:ref:`https://github.com/docker/compose-switch <compose_switch>`


Features
------------

- added support for COMPOSE_IGNORE_ORPHANS
- compose exec and compose down don't need the original compose file (require --project-name)
- render port ranges as a group
- container names use hyphens to produce a valid hostname
- add support for scale=0
- introduce --compatibility to support smooth(er) transition from V1
- introduce compose config --output flag (defaults to stdout)
- add support for build.network and build.extrahosts
- introduce compose up --attach
- fixes parsing compose file variables
- added support for multiline values in .env file
