

.. index::
   pair: docker-compose; 1.21.2 (2018-05-02)


.. _docker_compose_1_21:

=================================
docker-compose 1.21 (2018-04-09)
=================================

.. seealso:

   - https://github.com/docker/compose/tree/1.21.0




docker-compose 1.21.2 (2018-05-02)
=====================================


.. seealso::

   - https://github.com/docker/compose/tree/1.21.2

::

    $ docker-compose version

::

    docker-compose version 1.21.2, build a133471
    docker-py version: 3.3.0
    CPython version: 3.6.5
    OpenSSL version: OpenSSL 1.0.1t  3 May 2016
