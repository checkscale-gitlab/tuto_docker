

.. index::
   pair: docker-compose; versions


.. _docker_compose_versions:

=========================
docker-compose versions
=========================

- https://github.com/docker/compose/releases
- https://docs.docker.com/compose/

.. toctree::
   :maxdepth: 3

   2.3.3/2.3.3.rst
   2.0.0/2.0.0.rst
   1.24
   1.22
   1.21
