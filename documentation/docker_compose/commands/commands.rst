

.. index::
   pair: docker-compose ; overview
   pair: docker-compose ; commands
   pair: docker-compose ; build
   pair: docker-compose ; help
   pair: docker-compose ; up


.. _docker_compose_commnds:

==============================
docker-compose commands
==============================


.. seealso::

   - https://docs.docker.com/compose/overview
   - https://docs.docker.com/compose/reference
   - https://docs.docker.com/compose/compose-file/
   - https://docs.docker.com/compose/samples-for-compose






docker-compose help
======================

::

    docker-compose help


::

    Define and run multi-container applications with Docker.

    Usage:
      docker-compose [-f <arg>...] [options] [COMMAND] [ARGS...]
      docker-compose -h|--help

    Options:
      -f, --file FILE             Specify an alternate compose file
                                  (default: docker-compose.yml)
      -p, --project-name NAME     Specify an alternate project name
                                  (default: directory name)
      --verbose                   Show more output
      --log-level LEVEL           Set log level (DEBUG, INFO, WARNING, ERROR, CRITICAL)
      --no-ansi                   Do not print ANSI control characters
      -v, --version               Print version and exit
      -H, --host HOST             Daemon socket to connect to

      --tls                       Use TLS; implied by --tlsverify
      --tlscacert CA_PATH         Trust certs signed only by this CA
      --tlscert CLIENT_CERT_PATH  Path to TLS certificate file
      --tlskey TLS_KEY_PATH       Path to TLS key file
      --tlsverify                 Use TLS and verify the remote
      --skip-hostname-check       Don't check the daemon's hostname against the
                                  name specified in the client certificate
      --project-directory PATH    Specify an alternate working directory
                                  (default: the path of the Compose file)
      --compatibility             If set, Compose will attempt to convert deploy
                                  keys in v3 files to their non-Swarm equivalent

    Commands:
      build              Build or rebuild services
      bundle             Generate a Docker bundle from the Compose file
      config             Validate and view the Compose file
      create             Create services
      down               Stop and remove containers, networks, images, and volumes
      events             Receive real time events from containers
      exec               Execute a command in a running container
      help               Get help on a command
      images             List images
      kill               Kill containers
      logs               View output from containers
      pause              Pause services
      port               Print the public port for a port binding
      ps                 List containers
      pull               Pull service images
      push               Push service images
      restart            Restart services
      rm                 Remove stopped containers
      run                Run a one-off command
      scale              Set number of containers for a service
      start              Start services
      stop               Stop services
      top                Display the running processes
      unpause            Unpause services
      up                 Create and start containers
      version            Show the Docker-Compose version information



.. _docker_compose_build:

docker-compose build
======================

.. seealso::

   - https://docs.docker.com/compose/reference/build


::

    $ docker-compose help build

::


    Build or rebuild services.

    Services are built once and then tagged as `project_service`,
    e.g. `composetest_db`. If you change a service's `Dockerfile` or the
    contents of its build directory, you can run `docker-compose build` to rebuild it.

    Usage: build [options] [--build-arg key=val...] [SERVICE...]

    Options:
        --compress              Compress the build context using gzip.
        --force-rm              Always remove intermediate containers.
        --no-cache              Do not use cache when building the image.
        --pull                  Always attempt to pull a newer version of the image.
        -m, --memory MEM        Sets memory limit for the build container.
        --build-arg key=val     Set build-time variables for services.



docker-compose -f docker-compose.yml build django
----------------------------------------------------

::

    docker-compose -f docker-compose.yml build django





.. _docker_compose_ps:

docker-compose ps
===================

.. seealso::

   - https://docs.docker.com/compose/reference/up
   - :ref:`compose_container_status`


::

    $ docker-compose help ps

::

    List containers.

    Usage: ps [options] [SERVICE...]

    Options:
        -q, --quiet          Only display IDs
        --services           Display services
        --filter KEY=VAL     Filter services by a property



.. _docker_compose_up:

docker-compose up
===================

.. seealso::

   - https://docs.docker.com/compose/reference/up
   - :ref:`lauching_compose`


::

    docker-compose help up

::

    Builds, (re)creates, starts, and attaches to containers for a service.

    Unless they are already running, this command also starts any linked services.

    The `docker-compose up` command aggregates the output of each container. When
    the command exits, all containers are stopped. Running `docker-compose up -d`
    starts the containers in the background and leaves them running.

    If there are existing containers for a service, and the service's configuration
    or image was changed after the container's creation, `docker-compose up` picks
    up the changes by stopping and recreating the containers (preserving mounted
    volumes). To prevent Compose from picking up changes, use the `--no-recreate`
    flag.

    If you want to force Compose to stop and recreate all containers, use the
    `--force-recreate` flag.

    Usage: up [options] [--scale SERVICE=NUM...] [SERVICE...]

    Options:
        -d, --detach               Detached mode: Run containers in the background,
                                   print new container names. Incompatible with
                                   --abort-on-container-exit.
        --no-color                 Produce monochrome output.
        --quiet-pull               Pull without printing progress information
        --no-deps                  Don't start linked services.
        --force-recreate           Recreate containers even if their configuration
                                   and image haven't changed.
        --always-recreate-deps     Recreate dependent containers.
                                   Incompatible with --no-recreate.
        --no-recreate              If containers already exist, don't recreate
                                   them. Incompatible with --force-recreate and -V.
        --no-build                 Don't build an image, even if it's missing.
        --no-start                 Don't start the services after creating them.
        --build                    Build images before starting containers.
        --abort-on-container-exit  Stops all containers if any container was
                                   stopped. Incompatible with -d.
        -t, --timeout TIMEOUT      Use this timeout in seconds for container
                                   shutdown when attached or when containers are
                                   already running. (default: 10)
        -V, --renew-anon-volumes   Recreate anonymous volumes instead of retrieving
                                   data from the previous containers.
        --remove-orphans           Remove containers for services not defined
                                   in the Compose file.
        --exit-code-from SERVICE   Return the exit code of the selected service
                                   container. Implies --abort-on-container-exit.
        --scale SERVICE=NUM        Scale SERVICE to NUM instances. Overrides the
                                   `scale` setting in the Compose file if present.
