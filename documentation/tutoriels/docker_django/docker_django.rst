.. index::
   pair: Docker  ; Django (erroneousboat)
   pair: erroneousboat; docker-compose


.. _erroneousboat_django:

===========================================
Tutoriel **erroneousboat** Docker Django
===========================================


- https://github.com/erroneousboat/docker-django


tree
======

::

    git clone https://github.com/erroneousboat/docker-django


::

    Clonage dans 'docker-django'...
    remote: Enumerating objects: 536, done.
    remote: Total 536 (delta 0), reused 0 (delta 0), pack-reused 536
    Réception d'objets: 100% (536/536), 896.74 KiB | 449.00 KiB/s, fait.
    Résolution des deltas: 100% (265/265), fait.


::

    tree -L 4

::


    .
    ├── config
    │   ├── environment
    │   │   └── development.env
    │   └── webserver
    │       ├── localhost.crt
    │       ├── localhost.key
    │       └── nginx.tmpl
    ├── docker-compose.yml
    ├── LICENSE
    ├── README.md
    └── webapp
        ├── config
        │   ├── database-check.py
        │   ├── django-uwsgi.ini
        │   ├── requirements.txt
        │   └── start.sh
        ├── Dockerfile
        └── starter
            ├── manage.py
            └── starter
                ├── __init__.py
                ├── settings.py
                ├── urls.py
                └── wsgi.py

    7 directories, 17 files



docker-compose.yml
===================


- https://github.com/erroneousboat/docker-django/blob/master/docker-compose.yml


.. literalinclude:: docker-compose.yml
   :linenos:


webapp/Dockerfile
===================

.. seealso::

   - https://github.com/erroneousboat/docker-django/blob/master/webapp/Dockerfile


.. literalinclude:: webapp/Dockerfile
   :linenos:




README.md
===================

- https://github.com/erroneousboat/docker-django/blob/master/README.txt


.. literalinclude:: README.txt
   :linenos:
