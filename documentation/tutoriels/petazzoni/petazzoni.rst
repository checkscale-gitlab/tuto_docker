
.. index::
   pair: Jérôme Petazzoni ; Tutoriaux
   pair: Jérôme ; Petazzoni
   pair: Jérémy ; Garrouste
   pair: Docker ; Petazzoni
   pair: Docker ; Jérôme


.. _petazzoni:

=======================================================================================
Les conseils et formations de Jérôme Petazzoni
=======================================================================================


.. seealso::

   - https://avril2018.container.training/
   - :ref:`avril_2018`
   - https://github.com/jpetazzo/container.training
   - https://jpetazzo.github.io/2018/03/28/containers-par-ou-commencer/
   - https://github.com/jpetazzo
   - https://twitter.com/jpetazzo
   - https://twitter.com/jeremygarrouste
   - https://github.com/jpetazzo/container.training
   - https://training.play-with-docker.com
   - http://paris.container.training/intro.html
   - http://paris.container.training/kube.html
   - https://www.youtube.com/playlist?list=PLBAFXs0YjviLgqTum8MkspG_8VzGl6C07 (Docker)
   - https://www.youtube.com/playlist?list=PLBAFXs0YjviLrsyydCzxWrIP_1-wkcSHS (Kubernetes)





Se former, seul ou accompagné
================================

La communauté Docker est extrêmement riche en tutoriels divers pour
démarrer et aller plus loin.

Je recommande particulièrement les **labs** disponibles sur
training.play-with-docker.com_


.. _training.play-with-docker.com: https://training.play-with-docker.com

Si vous préférez être formé en personne, c’est aussi possible !

Publicité bien ordonnée commence par soi-même : en avril, j’organise
deux formations à Paris avec Jérémy Garrouste.

- Le 11 et 12 avril, :ref:`Introduction aux containers : de la pratique aux bonnes pratiques <avril_2018>`.
- Le 13 avril, `Introduction à l’orchestration : Kubernetes par l’exemple`_

La première formation vous permettra d’être à même d’accomplir les deux
premières étapes décrites dans le plan exposé plus haut.

La seconde formation vous permettra d’aborder les étapes 3 et 4.

Si vous voulez vous faire une idée de la qualité du contenu de ces
formations, vous pouvez consulter des vidéos et slides de formations
précédentes, par exemple :

- `journée d’introduction à Docker`_
- `demi-journée d’introduction à Kubernetes`_

Ces vidéos sont en anglais, mais les formations que je vous propose à
Paris en avril sont en français (le support de formation, lui, reste
en anglais).

Vous pouvez trouver d’autres vidéos, ainsi qu’une collection de supports
(slides etc.) sur http://container.training/.

Cela vous permettra de juger au mieux si ces formations sont adaptées
à votre besoin !

.. _`Introduction aux containers : de la pratique aux bonnes pratiques`:  http://paris.container.training/intro.html
.. _`Introduction à l’orchestration : Kubernetes par l’exemple`:  http://paris.container.training/kube.html
.. _`journée d’introduction à Docker`:  https://www.youtube.com/playlist?list=PLBAFXs0YjviLgqTum8MkspG_8VzGl6C07
.. _`demi-journée d’introduction à Kubernetes`: https://www.youtube.com/playlist?list=PLBAFXs0YjviLrsyydCzxWrIP_1-wkcSHS



Jérôme Petazzoni Container training
======================================

.. seealso::

   - https://github.com/jpetazzo/container.training
   - http://container.training/


Jérémy Garrouste
==================

.. seealso::

   - https://twitter.com/jeremygarrouste


Les slides de la formation d'avril 2018
==========================================

.. seealso:: https://avril2018.container.training/
