===========
README.txt
===========

Construire l'image
====================

::

    docker build -t id3centos7:0.1.1 .
    docker build -t id3centos7:0.1.2 .
    docker build -t id3centos7:0.1.4 .
    docker build -t id3centos7:0.1.6 .


Lancer le conteneur
====================

::

    docker run -v Y:\projects_id3\P5N001\XLOGCA135_tutorial_docker\tutorial_docker\tutoriels\centos7:/code --name id3centos7.1.1 -it id3centos7:0.1.1
    docker run --name id3centos7.1.2 -it id3centos7:0.1.2
    docker run --name id3centos7.0.1.3 -it id3centos7:0.1.3
    docker run --name id3centos7.0.1.4 -it id3centos7:0.1.4
    docker run --name id3centos7.0.1.6 -it id3centos7:0.1.6


Accéder au conteneur
======================

::

    docker exec -it id3centos7.1.2 bash



Arrêter un conteneur
=======================

::

    docker stop id3centos7.1.2
