====================
docker-pipenv-sample
=====================


Première version
=================


## Getting Started

* Build the image
```
    docker build -t docker-pipenv-sample .
```
* Run the image
```
    docker run -p 5000:5000 docker-pipenv-sample
```
## More information
Blog post on using [pip](https://pypi.python.org/pypi/pip) vs. [pipenv](https://github.com/kennethreitz/pipenv) together with Docker coming soon on [federschmidt.xyz](https://federschmidt.xyz).


Deuxième version
==================

Emploi de docker-compose.yml

::

    docker-compose up --build


::

    docker-compose down


::

    docker-compose exec flask bash
