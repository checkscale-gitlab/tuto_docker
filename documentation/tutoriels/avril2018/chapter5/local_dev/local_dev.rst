

.. index::
   pair: Local dev ; docker


.. _local_dev:

====================================================
Local development workflow with Docker
====================================================

.. seealso::

   - https://avril2018.container.training/intro.yml.html#335
   - https://avril2018.container.training/intro.yml.html#11
   - :ref:`petazzoni`



Objectives
============

At the end of this section, you will be able to:

- Share code between container and host.
- Use a simple local development workflow.


Containerized local development environments
==============================================

.. seealso::

   - https://avril2018.container.training/intro.yml.html#338


We want to solve the following issues:

- "Works on my machine"
- "Not the same version"
- "Missing dependency"

By using Docker containers, we will get a consistent development
environment.


Working on the "namer" application
=====================================

.. seealso::

   - https://avril2018.container.training/intro.yml.html#339


- We have to work on some application whose code is at:
  https://github.com/jpetazzo/namer.
- What is it? We don't know yet !
- Let's download the code.

::

    $ git clone https://github.com/jpetazzo/namer


Looking at the code
====================

.. seealso::

   - https://avril2018.container.training/intro.yml.html#340



::

	$ cd namer
	$ ls -1

::

	company_name_generator.rb
	config.ru
	docker-compose.yml
	Dockerfile
	Gemfile
