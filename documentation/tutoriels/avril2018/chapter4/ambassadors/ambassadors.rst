

.. index::
   pair: Ambassadors ; Container


.. _ambassadors:

====================================================
Ambassadors
====================================================

.. seealso::

   - https://avril2018.container.training/intro.yml.html#322
   - https://avril2018.container.training/intro.yml.html#10
   - :ref:`petazzoni`
