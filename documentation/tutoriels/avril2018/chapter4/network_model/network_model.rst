

.. index::
   pair: Network model ; Container


.. _container_model_drivers:

====================================================
Container network model
====================================================

.. seealso::

   - https://avril2018.container.training/intro.yml.html#288
   - https://avril2018.container.training/intro.yml.html#10
   - :ref:`petazzoni`
