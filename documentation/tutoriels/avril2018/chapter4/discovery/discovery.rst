

.. index::
   pair: Service discovery ; Container


.. _service_discovery:

====================================================
Service discovery with containers
====================================================

.. seealso::

   - https://avril2018.container.training/intro.yml.html#300
   - https://avril2018.container.training/intro.yml.html#10
   - :ref:`petazzoni`
