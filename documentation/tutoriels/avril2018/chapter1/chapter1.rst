

.. index::
   pair: Chapter1 ; Avril 2018


.. _chapter1_avril_2018:

====================================================
Chapter1 Avril 2018
====================================================

.. seealso::

   - https://avril2018.container.training/intro.yml.html#7
   - https://avril2018.container.training/intro.yml.html#16
   - :ref:`petazzoni`


.. toctree::
   :maxdepth: 4
