

.. index::
   pair: docker ; image
   pair: class ; image
   pair: container ; instance
   pair: images ; namespaces
   pair: root ; namespace
   pair: user ; namespace
   pair: self_hosted ; namespace
   pair: downloading ; images
   pair: image ; tags
   pair: version ; tag


.. _understand_docker_images:

=========================================
Understanding Docker images
=========================================

.. seealso::

   - https://avril2018.container.training/intro.yml.html#124






Objectives
=============

In this section, we will explain:

- What is an image.
- What is a layer.
- The various image namespaces.
- How to search and download images.
- Image tags and when to use them.


.. _docker_image:

What is an image ?
======================

.. seealso::

   - https://avril2018.container.training/intro.yml.html#127



Image = files + metadata

These files form the root filesystem of our container.

The metadata can indicate a number of things, e.g.:

- the author of the image
- the command to execute in the container when starting it
- environment variables to be set
- etc.

Images are made of layers, conceptually stacked on top of each other.

Each layer can add, change, and remove files and/or metadata.

Images can share layers to optimize disk usage, transfer times,
and memory use.


Differences between containers and images
============================================

.. seealso::

   - https://avril2018.container.training/intro.yml.html#129



- An image is a read-only filesystem.
- A container is an encapsulated set of processes running in a read-write
  copy of that filesystem.
- To optimize container boot time, copy-on-write is used instead of
  regular copy.
- docker run starts a container from a given image.

Let's give a couple of metaphors to illustrate those concepts.


Object-oriented programming
===============================

.. seealso::

   - https://avril2018.container.training/intro.yml.html#131

- Images are conceptually similar to classes.
- Layers are conceptually similar to inheritance.
- Containers are conceptually similar to instances.


Wait a minute
================

.. seealso::

   - https://avril2018.container.training/intro.yml.html#132



If an image is read-only, how do we change it?

- We don't.
- We create a new container from that image.
- Then we make changes to that container.
- When we are satisfied with those changes, we transform them into
  a new layer.
- A new image is created by stacking the new layer on top of the
  old image.


Creating the first images
=============================

.. seealso::

   - https://avril2018.container.training/intro.yml.html#134



There is a special empty image called scratch.
It allows to build from scratch.

The docker import command loads a tarball into Docker.

- The imported tarball becomes a standalone image.
- That new image has a single layer.

.. note: you will probably never have to do this yourself.


.. _creating_images:

Creating other images
=========================

.. seealso::

   - https://avril2018.container.training/intro.yml.html#135


docker commit
---------------

- Saves all the changes made to a container into a new layer.
- Creates a new image (effectively a copy of the container).

docker build
---------------

- Performs a repeatable build sequence.
- This is the preferred method!

We will explain both methods in a moment.


.. _images_namespaces:

Images namespaces
====================

.. seealso::

   - https://avril2018.container.training/intro.yml.html#136


There are three namespaces:

- Official images:

  - e.g. ubuntu, busybox ...

- User (and organizations) images:

  - e.g. jpetazzo/clock

- Self-hosted images:

  - e.g. registry.example.com:5000/my-private/image

Let's explain each of them.


.. _root_namespace:

Root namespace
=================

.. seealso::

   - https://avril2018.container.training/intro.yml.html#137


The root namespace is for official images. They are put there by
Docker Inc., but they are generally authored and maintained
by third parties.

Those images include:

- Small, "swiss-army-knife" images like busybox.
- Distro images to be used as bases for your builds, like ubuntu, fedora...
- Ready-to-use components and services, like redis, postgresql...

.. _user_namespace:

User namespace
================

.. seealso::

   - https://avril2018.container.training/intro.yml.html#138


The user namespace holds images for Docker Hub users and organizations.

For example:

- jpetazzo/clock

The Docker Hub user is:

- jpetazzo

The image name is:

- clock


.. _self_hosted_namespace:

Self-Hosted namespace
========================

.. seealso::

   - https://avril2018.container.training/intro.yml.html#139


This namespace holds images which are not hosted on Docker Hub, but on
third party registries.

They contain the hostname (or IP address), and optionally the port,
of the registry server.

For example:

- localhost:5000/wordpress

- **localhost:5000** is the host and port of the registry
- **wordpress** is the name of the image


How do you store and manage images ?
=======================================

.. seealso::

   - https://avril2018.container.training/intro.yml.html#140


Images can be stored:

- On your Docker host.
- In a Docker registry.

You can use the Docker client to download (pull) or upload (push) images.

To be more accurate: you can use the Docker client to tell a Docker
Engine to push and pull images to and from a registry.


.. _showing_images:

Showing current images
=========================

.. seealso::

   - https://avril2018.container.training/intro.yml.html#141


Let's look at what images are on our host now.


::

    docker images

::

	REPOSITORY                  TAG                 IMAGE ID            CREATED             SIZE
	ch4-message-board-app_web   latest              376512737492        21 hours ago        1.04GB
	gdevops/django36_ch4        latest              b44a8c214cdf        22 hours ago        1.04GB
	postgres                    10.4                61d053fc271c        4 days ago          236MB
	busybox                     latest              8c811b4aec35        6 days ago          1.15MB
	python                      3.6                 29d2f3226daf        3 weeks ago         911MB
	ubuntu                      latest              452a96d81c30        4 weeks ago         79.6MB
	jpetazzo/clock              latest              12068b93616f        3 years ago         2.43MB



.. _searching_images:

Searching for images
=======================

.. seealso::

   - https://avril2018.container.training/intro.yml.html#142


We cannot list all images on a remote registry, but we can search for
a specific keyword:

::

    $ docker search marathon

::

	NAME                                   DESCRIPTION                                     STARS               OFFICIAL            AUTOMATED
	mesosphere/marathon                    A cluster-wide init and control system for s…   106                                     [OK]
	mesoscloud/marathon                    Marathon                                        31                                      [OK]
	mesosphere/marathon-lb                 Script to update haproxy based on marathon s…   22                                      [OK]
	mesosphere/marathon-lb-autoscale       Autoscale your apps on Marathon                 5                                       [OK]
	thefactory/marathon                    Tagged images of each Mesos Marathon release    4                                       [OK]
	brndnmtthws/marathon-lb-autoscale      Marathon-lb autoscale demo                      3                                       [OK]
	mesoscloud/haproxy-marathon            [DEPRECATED] Generate HAProxy configuration …   3                                       [OK]
	f5networks/marathon-asp-ctlr           Official container repository for F5 Maratho…   3
	bobrik/marathon-tcp-haproxy                                                            2                                       [OK]
	tobilg/marathon-slack                  Listen to Marathon's Event Bus and send sele…   2                                       [OK]
	f5networksdevel/marathon-bigip-ctlr    Container repository for development images …   1
	tobilg/gitlab-ci-runner-marathon       A customized Docker image for running scalab…   1                                       [OK]
	eduser25/pg-marathon-watcher           PG Marathon watcher application for Maratho/…   1
	vidazoohub/marathon-rabbit-autoscale   autoscale marathon tasks based on rabbitmq q…   1                                       [OK]
	gettyimages/marathon_exporter          Marathon metrics exporter for Prometheus        0
	skytix/marathon-consul                 Consul service registration daemon that moni…   0
	heww/marathon-dns                      dns for marathon apps                           0
	jeffdecola/resource-marathon-deploy    A Concourse resource type that deploys an AP…   0
	ryanmehta/marathon-resource                                                            0
	praekeltfoundation/marathon-acme       Automatically manage ACME certificates for a…   0                                       [OK]
	ckaznocha/marathon-resource            A Concourse resource to deploy applications …   0
	quintoandar/drone-marathon             Drone plugin to create marathon deployments     0                                       [OK]
	jamiecressey89/marathon-zookeeper      Zookeeper image that uses Marathon's API for…   0                                       [OK]
	alenkacz/marathon-rabbitmq-autoscale   Autoscaling capabilities for apps running in…   0                                       [OK]
	mrbobbytables/marathon                 Marathon Mesos Framework container.             0                                       [OK]



- "Stars" indicate the popularity of the image.
- "Official" images are those in the root namespace.
- "Automated" images are built automatically by the Docker Hub.

(This means that their build recipe is always available.)


.. _downloading_images:

Downloading images
======================

.. seealso::

   - https://avril2018.container.training/intro.yml.html#143



There are two ways to download images.

- Explicitly, with docker pull.
- Implicitly, when executing docker run and the image is not found
  locally.


.. _pulling_image:

Pulling an image
==================


.. seealso::

   - https://avril2018.container.training/intro.yml.html#144


::

    $ docker pull debian:jessie

::

	jessie: Pulling from library/debian
	3d77ce4481b1: Pull complete
	Digest: sha256:f29d0c98d94d6b2169c740d498091a9a8545fabfa37f2072b43a4361c10064fc
	Status: Downloaded newer image for debian:jessie


In this example, **:jessie** indicates which exact version of Debian
we would like. **It is a version tag**.


.. _image_and_tags:

Image and tags
================

.. seealso::

   - https://avril2018.container.training/intro.yml.html#145


- Images can have tags.
- Tags define image versions or variants.
- docker pull ubuntu will refer to ubuntu:latest.
- The :latest tag is generally updated often.


When to (not) use tags
=========================

.. seealso::

   - https://avril2018.container.training/intro.yml.html#146


Don't specify tags
---------------------

- When doing rapid testing and prototyping.
- When experimenting.
- When you want the latest version.

Do specify tags
-----------------

- When recording a procedure into a script.
- When going to production.
- To ensure that the same version will be used everywhere.
- To ensure repeatability later.


Section summary
=================

.. seealso::

   - https://avril2018.container.training/intro.yml.html#147


We've learned how to:

- Understand images and layers.
- Understand Docker image namespacing.
- Search and download images.
