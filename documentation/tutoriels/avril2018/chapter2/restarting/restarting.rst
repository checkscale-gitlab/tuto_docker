

.. index::
   pair: attaching ; container
   pair: detaching ; container
   pair: restarting ; containers
   pair: detach ; sequence
   pair: detach-keys ; container
   pair: _container ; restarting


.. _restarting_containers:

=========================================
Restarting and attaching to containers
=========================================





Introduction
==============

.. seealso::

   - https://avril2018.container.training/intro.yml.html#112


We have started containers in the foreground, and in the background.

In this chapter, we will see how to:

- Put a container in the background.
- Attach to a background container to bring it to the foreground.
- Restart a stopped container.

Background and foreground
===========================

.. seealso::

   - https://avril2018.container.training/intro.yml.html#114


The distinction between foreground and background containers is arbitrary.

From Docker's point of view, all containers are the same.

All containers run the same way, whether there is a client attached to
them or not.

It is always possible to detach from a container, and to reattach to a container.

Analogy: attaching to a container is like plugging a keyboard and screen
to a physical server.


.. _container_detach_keys:

Detaching from a container
============================

.. seealso::

   - https://avril2018.container.training/intro.yml.html#115


If you have started an interactive container (with option -it), you can
detach from it.

The "detach" sequence is ^P^Q.

Otherwise you can detach by killing the Docker client.

(But not by hitting ^C, as this would deliver SIGINT to the container.)

What does -it stand for?

- `-t` means "allocate a terminal."
- `-i` means "connect stdin to the terminal."


Specifying a custom detach sequence
======================================

.. seealso::

   - https://avril2018.container.training/intro.yml.html#116


- You don't like ^P^Q? No problem!
- You can change the sequence with docker run --detach-keys.
- This can also be passed as a global option to the engine.

Start a container with a custom detach command::

    $ docker run -ti --detach-keys ctrl-x,x jpetazzo/clock

Detach by hitting ^X x. (This is ctrl-x then x, not ctrl-x twice!)

Check that our container is still running::

    $ docker ps -l

::

	CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS               NAMES
	16c9e8ab42c3        jpetazzo/clock      "/bin/sh -c 'while d…"   26 seconds ago      Up 24 seconds                           unruffled_joliot


.. _attach_container:

Attaching to a container
===========================

.. seealso::

   - https://avril2018.container.training/intro.yml.html#117


You can attach to a container

::

    $ docker attach <containerID>

The container must be running.
There can be multiple clients attached to the same container.
If you don't specify --detach-keys when attaching, it defaults back
to **^P^Q**.

Try it on our previous container

::

    $ docker attach $(docker ps -lq)

Check that **^X x** doesn't work, but **^P ^Q** does.


Detaching from non-interactive containers
============================================

.. seealso::

   - https://avril2018.container.training/intro.yml.html#118



Warning: if the container was started without -it...

	- You won't be able to detach with ^P^Q.
	- If you hit ^C, the signal will be proxied to the container.

Remember: you can always detach by killing the Docker client.


.. _container_restarting:

Restarting a container
=========================

When a container has exited, it is in stopped state.

It can then be restarted with the start command.

::

    $ docker start <yourContainerID>

The container will be restarted using the same options you launched
it with.

You can re-attach to it if you want to interact with it.

::

    $ docker attach <yourContainerID>

Use docker ps -a to identify the container ID of a previous jpetazzo/clock container,
and try those commands.
