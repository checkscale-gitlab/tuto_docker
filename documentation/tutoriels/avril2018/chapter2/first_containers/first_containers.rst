

.. index::
   pair: first ; containers

.. _first_containers:

======================
Our first containers
======================

.. seealso::

   - https://avril2018.container.training/intro.yml.html#79
   - :ref:`petazzoni`





Hello World
=============

.. seealso::

   - https://avril2018.container.training/intro.yml.html#82
   - :ref:`docker_run`

::

    # docker run busybox echo hello world


::

	Unable to find image 'busybox:latest' locally
	latest: Pulling from library/busybox
	07a152489297: Pull complete
	Digest: sha256:141c253bc4c3fd0a201d32dc1f493bcf3fff003b6df416dea4f41046e0f37d47
	Status: Downloaded newer image for busybox:latest
	hello world



Starting another container
============================

.. seealso::

   - https://avril2018.container.training/intro.yml.html#92
