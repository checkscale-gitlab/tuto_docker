

.. index::
   pair: Publishing ; images
   pair: Docker ; hub


.. _publishing_images:

====================================================
Publishing images to the Docker Hub
====================================================

.. seealso::

   - https://avril2018.container.training/intro.yml.html#213
   - https://avril2018.container.training/intro.yml.html#9
   - :ref:`petazzoni`






Publishing images to the Docker Hub
======================================

.. seealso::

   - https://avril2018.container.training/intro.yml.html#214


We have built our first images.

We can now publish it to the Docker Hub!

You don't have to do the exercises in this section, because they require
an account on the Docker Hub, and we don't want to force anyone to
create one.

Note, however, that creating an account on the Docker Hub is free
(and doesn't require a credit card), and hosting public images is
free as well.


.. _login_dockerhub:

Logging into our Docker Hub account
======================================

.. seealso::

   - https://avril2018.container.training/intro.yml.html#215


This can be done from the Docker CLI::

    docker login

::

    Login with your Docker ID to push and pull images from Docker Hub.
    If you don't have a Docker ID, head over to https://hub.docker.com
    to create one.

::

    Username: id3pvergain

::

    Password:

::

    Login Succeeded


Image tags and registry addresses
===================================

.. seealso::

   - https://avril2018.container.training/intro.yml.html#216


- Docker images tags are like Git tags and branches.
- They are like bookmarks pointing at a specific image ID.
- Tagging an image doesn't rename an image: it adds another tag.
- When pushing an image to a registry, the registry address is in the tag.
- Example: registry.example.net:5000/image
- What about Docker Hub images?


Image tags and registry addresses
====================================

.. seealso::

   - https://avril2018.container.training/intro.yml.html#217


::

	- Docker images tags are like Git tags and branches.
	- They are like bookmarks pointing at a specific image ID.
	- Tagging an image doesn't rename an image: it adds another tag.
	- When pushing an image to a registry, the registry address is in the tag.
    - Example: registry.example.net:5000/image


- What about Docker Hub images?
- jpetazzo/clock is, in fact, index.docker.io/jpetazzo/clock
- ubuntu is, in fact, library/ubuntu, i.e. index.docker.io/library/ubuntu


Tagging an image to push it on the Hub
========================================

.. seealso::

   - https://avril2018.container.training/intro.yml.html#218
   - https://avril2018.container.training/intro.yml.html#219


figlet Dockerfile
------------------

::

	# cat Dockerfile

::

	FROM ubuntu
	RUN apt-get update
	RUN ["apt-get", "install", "figlet"]
	ENTRYPOINT ["figlet", "-f", "script"]
	CMD ["hello world"]


Let's tag our figlet image (or any other to our liking)

::

    docker tag figlet id3pvergain/figlet

::

    docker push id3pvergain/figlet


::

	[root@intranet-dev myimage]# docker tag figlet id3pvergain/figlet
	[root@intranet-dev myimage]# docker push id3pvergain/figlet
	The push refers to repository [docker.io/id3pvergain/figlet]
	6a460659e0ae: Pushed
	3023de562a6f: Pushed
	059ad60bcacf: Mounted from library/ubuntu
	8db5f072feec: Mounted from library/ubuntu
	67885e448177: Mounted from library/ubuntu
	ec75999a0cb1: Mounted from library/ubuntu
	65bdd50ee76a: Mounted from library/ubuntu
	latest: digest: sha256:b239196e33c151a85c6bea76bb3eecaedea8ea43d811d0d3aba7ed32efa9e919 size: 1779


.. figure:: figlet_publish.png
   :align: center




Anybody can now docker run id3pvergain/figlet anywhere.
