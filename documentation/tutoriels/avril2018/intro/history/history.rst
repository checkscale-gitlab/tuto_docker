

.. index::
   pair: History ; Avril 2018


.. _history_avril_2018:

====================================================
History of containers ... and Docker
====================================================

.. seealso::

   - https://avril2018.container.training/intro.yml.html#44
   - https://avril2018.container.training/intro.yml.html#1
   - :ref:`petazzoni`


.. contents::
   :depth: 4


First experimentations
=========================

.. seealso::

   - https://avril2018.container.training/intro.yml.html#46


- IBM VM/370 (1972)
- Linux VServers (2001)
- Solaris Containers (2004)
- FreeBSD jails (1999)

Containers have been around for a very long time indeed.


The VPS age (until 2007-2008)
===============================

.. seealso::

   - https://avril2018.container.training/intro.yml.html#47


.. figure:: containers-as-lightweight-vms.png
   :align: center


Containers = cheaper than VMs
===============================

.. seealso::

   - https://avril2018.container.training/intro.yml.html#48


- Users: hosting providers.
- Highly specialized audience with strong ops culture.


The PAAS period (2008-2013)
==============================

.. seealso::

   - https://avril2018.container.training/intro.yml.html#49


.. figure:: heroku-first-homepage.png
   :align: center


Containers = easier than VMs
==============================

.. seealso::

   - https://avril2018.container.training/intro.yml.html#50



- I can't speak for Heroku, but containers were (one of) dotCloud's secret weapon
- dotCloud was operating a PaaS, using a custom container engine.
- This engine was based on OpenVZ (and later, LXC) and AUFS.
- It started (circa 2008) as a single Python script.
- By 2012, the engine had multiple (~10) Python components.
- (and ~100 other micro-services!)
- End of 2012, dotCloud refactors this container engine.

The codename for this project is *Docker*.


First public release of Docker
================================

.. seealso::

   - https://avril2018.container.training/intro.yml.html#51

- March 2013, PyCon, Santa Clara:
  "Docker" is shown to a public audience for the first time.
- It is released with an open source license.
- Very positive reactions and feedback!
- The dotCloud team progressively shifts to Docker development.
- The same year, dotCloud changes name to Docker.
- In 2014, the PaaS activity is sold.


Docker early days (2013-2014)
================================

.. seealso::

   - https://avril2018.container.training/intro.yml.html#52


First users of Docker
=======================

.. seealso::

   - https://avril2018.container.training/intro.yml.html#53



- PAAS builders (Flynn, Dokku, Tsuru, Deis...)
- PAAS users (those big enough to justify building their own)
- CI platforms
- developers, developers, developers, developers


Positive feedback loop
========================

.. seealso::

   - https://avril2018.container.training/intro.yml.html#54



- In 2013, the technology under containers (cgroups, namespaces,
  copy-on-write storage...)  had many blind spots.
- The growing popularity of Docker and containers exposed many bugs.
- As a result, those bugs were fixed, resulting in better stability
  for containers.
- Any decent hosting/cloud provider can run containers today.
- Containers become a great tool to deploy/move workloads to/from on-prem/cloud.


Maturity (2015-2016)
=====================

.. seealso::

   - https://avril2018.container.training/intro.yml.html#55



Docker becomes an industry standard
=====================================

.. seealso::

   - https://avril2018.container.training/intro.yml.html#56



- Docker reaches the symbolic 1.0 milestone.
- Existing systems like Mesos and Cloud Foundry add Docker support.
- Standardization around the OCI (Open Containers Initiative).
- Other container engines are developed.
- Creation of the CNCF (Cloud Native Computing Foundation).


Docker becomes a platform
============================

.. seealso::

   - https://avril2018.container.training/intro.yml.html#56


The initial container engine is now known as **Docker Engine**

Other tools are added:

- Docker Compose (formerly "Fig")
- Docker Machine
- Docker Swarm
- Kitematic
- Docker Cloud (formerly "Tutum")
- Docker Datacenter
- etc.

Docker Inc. launches commercial offers.
