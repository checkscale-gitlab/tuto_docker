
.. index::
   pair: Part2 ; Get Started



.. _get_started_part2:

==========================================================
Get started Part2 : Containers
==========================================================

.. seealso::

   - https://docs.docker.com/get-started/part2/
   - https://hub.docker.com/
   - https://hub.docker.com/u/id3pvergain/
   - :ref:`tutos_docker_windows`





Prérequis
===========

Ne pas oublier de démarrer le serveur docker.

::

    Y:\projects_id3\P5N001\XLOGCA135_tutorial_docker\tutorial_docker\tutoriels\get_started\part2>docker build -t friendlyhello .

::

    error during connect: Post http://%2F%2F.%2Fpipe%2Fdocker_engine/v1.35/build?buildargs=%7B%7D&cachefrom=%5B%5D&cgroupparent=&cpuperiod=0&cpuquota=0&cpusetcpus=&cpusetmems=&cpushares=0&dockerfile=Dockerfile&labels=%7B%7D&memory=0&memswap=0&networkmode=default&rm=1&session=503be270159342059d8cbfa34d94c9f1e312558a1dcef2ef4369cb0b440ad6a3&shmsize=0&t=friendlyhello&target=&ulimits=null:
    open //./pipe/docker_engine: Le fichier spécifié est introuvable.
    In the default daemon configuration on Windows, the docker client
    must be run elevated to connect.
    This error may also indicate that the docker daemon is not running.


Build the app: docker build -t friendlyhello .
=================================================


::
    docker build -t friendlyhello .

::

	Sending build context to Docker daemon  7.168kB
	Step 1/7 : FROM python:2.7-slim
	2.7-slim: Pulling from library/python
	c4bb02b17bb4: Pull complete
	c5c896dce5ee: Pull complete
	cf210b898cc6: Pull complete
	5117cef49bdb: Pull complete
	Digest: sha256:22112f2295fe9ea84b72e5344af73a2580a47b1014a1f4c58eccf6095b7ea18f
	Status: Downloaded newer image for python:2.7-slim
	 ---> 4fd30fc83117
	Step 2/7 : WORKDIR /app
	Removing intermediate container 8ed2ad0d0958
	 ---> 7400c8709865
	Step 3/7 : ADD . /app
	 ---> 728e5124216a
	Step 4/7 : RUN pip install --trusted-host pypi.python.org -r requirements.txt
	 ---> Running in 847d00a0831e
	Collecting Flask (from -r requirements.txt (line 1))
	  Downloading Flask-0.12.2-py2.py3-none-any.whl (83kB)
	Collecting Redis (from -r requirements.txt (line 2))
	  Downloading redis-2.10.6-py2.py3-none-any.whl (64kB)
	Collecting itsdangerous>=0.21 (from Flask->-r requirements.txt (line 1))
	  Downloading itsdangerous-0.24.tar.gz (46kB)
	Collecting Jinja2>=2.4 (from Flask->-r requirements.txt (line 1))
	  Downloading Jinja2-2.10-py2.py3-none-any.whl (126kB)
	Collecting Werkzeug>=0.7 (from Flask->-r requirements.txt (line 1))
	  Downloading Werkzeug-0.14.1-py2.py3-none-any.whl (322kB)
	Collecting click>=2.0 (from Flask->-r requirements.txt (line 1))
	  Downloading click-6.7-py2.py3-none-any.whl (71kB)
	Collecting MarkupSafe>=0.23 (from Jinja2>=2.4->Flask->-r requirements.txt (line 1))
	  Downloading MarkupSafe-1.0.tar.gz
	Building wheels for collected packages: itsdangerous, MarkupSafe
	  Running setup.py bdist_wheel for itsdangerous: started
	  Running setup.py bdist_wheel for itsdangerous: finished with status 'done'
	  Stored in directory: /root/.cache/pip/wheels/fc/a8/66/24d655233c757e178d45dea2de22a04c6d92766abfb741129a
	  Running setup.py bdist_wheel for MarkupSafe: started
	  Running setup.py bdist_wheel for MarkupSafe: finished with status 'done'
	  Stored in directory: /root/.cache/pip/wheels/88/a7/30/e39a54a87bcbe25308fa3ca64e8ddc75d9b3e5afa21ee32d57
	Successfully built itsdangerous MarkupSafe
	Installing collected packages: itsdangerous, MarkupSafe, Jinja2, Werkzeug, click, Flask, Redis
	Successfully installed Flask-0.12.2 Jinja2-2.10 MarkupSafe-1.0 Redis-2.10.6 Werkzeug-0.14.1 click-6.7 itsdangerous-0.24
	Removing intermediate container 847d00a0831e
	 ---> 3dc371ea405c
	Step 5/7 : EXPOSE 80
	 ---> Running in 0f4b33dbfcd0
	Removing intermediate container 0f4b33dbfcd0
	 ---> d1d59914b22b
	Step 6/7 : ENV NAME World
	 ---> Running in a742b8e9bddb
	Removing intermediate container a742b8e9bddb
	 ---> b79587f955c5
	Step 7/7 : CMD ["python", "app.py"]
	 ---> Running in f9c7ee2841c0
	Removing intermediate container f9c7ee2841c0
	 ---> ed5b70620e49
	Successfully built ed5b70620e49
	Successfully tagged friendlyhello:latest
	SECURITY WARNING: You are building a Docker image from Windows against
	a non-Windows Docker host. All files and directories added to build
	context will have '-rwxr-xr-x' permissions.
	It is recommended to double check and reset permissions for sensitive
	files and directories.



docker images
==============

::

    docker images

::

	REPOSITORY                 TAG                 IMAGE ID            CREATED             SIZE
	friendlyhello              latest              ed5b70620e49        10 minutes ago      148MB
	wordpress                  latest              28084cde273b        6 days ago          408MB
	centos                     latest              ff426288ea90        6 days ago          207MB
	nginx                      latest              3f8a4339aadd        2 weeks ago         108MB
	python                     2.7-slim            4fd30fc83117        4 weeks ago         138MB
	hello-world                latest              f2a91732366c        7 weeks ago         1.85kB
	docker4w/nsenter-dockerd   latest              cae870735e91        2 months ago        187kB


Run the app:  docker run -p 4000:80 friendlyhello
====================================================

::

    Y:\projects_id3\P5N001\XLOGCA135_tutorial_docker\tutorial_docker\tutoriels\part2>docker run -p 4000:80 friendlyhello

::

    * Running on http://0.0.0.0:80/ (Press CTRL+C to quit)




.. figure:: flask_redis.png
   :align: center

   http://localhost:4000/




docker container ls
=====================


::

    Y:\projects_id3\P5N001\XLOGCA135_tutorial_docker\tutorial_docker>docker container ls

::

	CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS                  NAMES
	06193b763075        friendlyhello       "python app.py"     41 minutes ago      Up 41 minutes       0.0.0.0:4000->80/tcp   boring_goodall




docker container stop 06193b763075
====================================

::

    docker container stop 06193b763075

::

    06193b763075




Tag the image: docker tag friendlyhello id3pvergain/get-started:part2
========================================================================

::

    docker tag friendlyhello id3pvergain/get-started:part2


Publish the image
==================

::

    docker push id3pvergain/get-started:part2


::

	The push refers to repository [docker.io/id3pvergain/get-started]
	af88fcfe37d7: Pushed
	b13ed1abc5b3: Pushed
	150ac820623b: Pushed
	94b0b6f67798: Mounted from library/python
	e0c374004259: Mounted from library/python
	56ee7573ea0f: Mounted from library/python
	cfce7a8ae632: Mounted from library/python
	part2: digest: sha256:1afb795959667db38cc58581d8d455ce10eff78be3cce18560ba887fb6f8c920 size: 1788



Once complete, the results of this upload are publicly available.
If you log in to Docker Hub, you will see the new image there, with
its pull command.

.. figure:: ../../../../images/login_id3pvergain.png
   :align: center

   https://hub.docker.com/u/id3pvergain/




Pull and run the image from the remote repository
====================================================

.. seealso::

   - https://docs.docker.com/get-started/part2/#pull-and-run-the-image-from-the-remote-repository

From now on, you can use docker run and run your app on any machine
with this command::

    docker run -p 4000:80 id3pvergain/get-started:part2


If the image isn’t available locally on the machine, Docker will pull it
from the repository.


Here is a list of the basic Docker commands from this page, and some
related ones if you’d like to explore a bit before moving on.

::

	docker build -t friendlyhello .  # Create image using this directory's Dockerfile
	docker run -p 4000:80 friendlyhello  # Run "friendlyname" mapping port 4000 to 80
	docker run -d -p 4000:80 friendlyhello         # Same thing, but in detached mode
	docker container ls                                # List all running containers
	docker container ls -a             # List all containers, even those not running
	docker container stop <hash>           # Gracefully stop the specified container
	docker container kill <hash>         # Force shutdown of the specified container
	docker container rm <hash>        # Remove specified container from this machine
	docker container rm $(docker container ls -a -q)         # Remove all containers
	docker image ls -a                             # List all images on this machine
	docker image rm <image id>            # Remove specified image from this machine
	docker image rm $(docker image ls -a -q)   # Remove all images from this machine
	docker login             # Log in this CLI session using your Docker credentials
	docker tag <image> username/repository:tag  # Tag <image> for upload to registry
	docker push username/repository:tag            # Upload tagged image to registry
	docker run username/repository:tag                   # Run image from a registry
