
.. index::
   pair: Part4 ; Get Started
   pair: Docker ; swarm
   pair: Dockerized cluster ; swarm


.. _get_started_part4:

==========================================================
Get started Part4 : **swarms**
==========================================================

.. seealso::

   - https://docs.docker.com/get-started/part4/





Introduction
==============

In :ref:`part 3 <get_started_part3>`, you took an app you wrote
in :ref:`part 2 <get_started_part2>`, and defined how it should run in
production by turning it into a service, scaling it up 5x in the process.

Here in part 4, you deploy this application onto a cluster, running it
on multiple machines.

Multi-container, multi-machine applications are made possible by joining
multiple machines into a *Dockerized cluster* called a **swarm**.


Understanding Swarm clusters
==============================

A swarm is a group of machines that are running Docker and joined into
a cluster.
After that has happened, you continue to run the Docker commands you’re
used to, but now they are executed on a cluster by a **swarm manager**.

The machines in a swarm can be physical or virtual.
After joining a swarm, they are referred to as **nodes**.

Swarm managers can use several strategies to run containers, such as
*emptiest node* – which fills the least utilized machines with containers.
Or *global*, which ensures that each machine gets exactly one instance
of the specified container.
You instruct the swarm manager to use these strategies in the Compose file,
just like the one you have already been using.

**Swarm managers** are the only machines in a swarm that can execute your
commands, or authorize other machines to join the swarm as workers.
Workers are just there to provide capacity and do not have the authority
to tell any other machine what it can and cannot do.

Up until now, you have been using Docker in a single-host mode on your
local machine. But Docker also can be switched into swarm mode, and
that’s what enables the use of swarms.
Enabling **swarm mode** instantly makes the current machine a swarm manager.
From then on, Docker will run the commands you execute on the swarm
you’re managing, rather than just on the current machine.


Set up your swarm
===================

A swarm is made up of multiple nodes, which can be either physical or
virtual machines.
The basic concept is simple enough: run docker swarm init to enable
swarm mode and make your current machine a swarm manager, then run
docker swarm join on other machines to have them join the swarm as
workers.

Choose a tab below to see how this plays out in various contexts.
We’ll use VMs to quickly create a two-machine cluster and turn it
into a swarm.



Encore Bloqué
==============

.. seealso::

   - https://github.com/boot2docker/boot2docker/releases/download/v18.01.0-ce/boot2docker.iso

::

    PS C:/WINDOWS/system32> docker-machine create -d hyperv --hyperv-virtual-switch "myswitch" myvm1

::


	PS C:/WINDOWS/system32> docker-machine create -d hyperv --hyperv-virtual-switch "myswitch" myvm1
	Creating CA: C:/Users/compadm/.docker/machine/certs/ca.pem
	Creating client certificate: C:/Users/compadm/.docker/machine/certs/cert.pem
	Running pre-create checks...
	(myvm1) Image cache directory does not exist, creating it at C:/Users/compadm/.docker/machine/cache...
	(myvm1) No default Boot2Docker ISO found locally, downloading the latest release...
	(myvm1) Latest release for github.com/boot2docker/boot2docker is v18.01.0-ce
	(myvm1) Downloading C:/Users/compadm/.docker/machine/cache/boot2docker.iso from https://github.com/boot2docker/boot2dock
	er/releases/download/v18.01.0-ce/boot2docker.iso...
	Error with pre-create check: "Get https://github-production-release-asset-2e65be.s3.amazonaws.com/14930729/634fb5b0-f6ac-11e7-8f12-e1c4544a979b?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=A
	KIAIWNJYAX4CSVEH53A%2F20180115%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Date=20180115T134730Z&X-Amz-Expires=300&X-Amz-Signature=5efdfe365c94b790f1a95579a7f424a0731be82a19a2d806340d18c5608577be&X-Amz
	-SignedHeaders=host&actor_id=0&response-content-disposition=attachment%3B%20filename%3Dboot2docker.iso&response-content-type=application%2Foctet-stream: read tcp 10.0.40.41:55806->54.231.48.184:4
	43: wsarecv: Une tentative de connexion a échoué car le parti connecté n’a pas répondu convenablement au-delà d’une certaine durée ou une connexion établie a échoué car l’hôte de connexion n’a pa
	s répondu."





.. warning:: impossible d'accéder au stockage `Amazon S3`_.



.. _`Amazon S3`:  https://fr.wikipedia.org/wiki/Amazon_S3



Solution
---------

Téléchargement à la maison et copie manuelle sous
:file:`C:/Users/compadm/.docker/machine/cache`.

.. figure:: boot2docker_iso_cache.png
   :align: center

   https://github.com/boot2docker/boot2docker/releases/download/v18.01.0-ce/boot2docker.iso
