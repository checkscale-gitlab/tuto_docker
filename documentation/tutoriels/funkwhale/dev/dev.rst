

.. index::
   pair: dev.yml  ; docker-compose.yml (Funkwhale)


.. _funkwhale_dev_dockercompose:

===========================================
Funkwhale dev.yml docker-compose
===========================================






.. seealso::

   - https://dev.funkwhale.audio/funkwhale/funkwhale





Layout
========

::

    tree -L 2

::


    4 drwxr-xr-x 12   4096 févr. 28 15:43 .
    4 drwxr-xr-x 41   4096 févr. 28 14:32 ..
    4 drwxr-xr-x  7   4096 févr. 28 14:33 api
    84 -rw-r--r--  1  85413 févr. 28 14:33 CHANGELOG
    4 drwxr-xr-x  3   4096 févr. 28 14:33 changes
    24 -rw-r--r--  1  23892 févr. 28 14:33 CONTRIBUTING.rst
    4 -rw-r--r--  1     14 févr. 28 14:33 CONTRIBUTORS.txt
    4 drwxr-xr-x  2   4096 févr. 28 14:33 demo
    4 drwxr-xr-x  3   4096 févr. 28 14:33 deploy
    4 -rw-r--r--  1   3972 févr. 28 14:33 dev.yml
    4 drwxr-xr-x  4   4096 févr. 28 14:33 docker
    4 -rw-r--r--  1    840 févr. 28 14:33 .dockerignore
    4 drwxr-xr-x  7   4096 févr. 28 14:33 docs
    4 -rw-r--r--  1    441 févr. 28 14:33 .editorconfig
    4 -rw-r--r--  1    596 févr. 28 14:33 .env.dev
    4 drwxr-xr-x  7   4096 févr. 28 14:33 front
    4 drwxr-xr-x  8   4096 mars   1 16:36 .git
    4 -rw-r--r--  1     12 févr. 28 14:33 .gitattributes
    4 -rw-r--r--  1   1292 févr. 28 14:33 .gitignore
    4 drwxr-xr-x  4   4096 févr. 28 14:33 .gitlab
    8 -rw-r--r--  1   7364 févr. 28 14:33 .gitlab-ci.yml
    36 -rw-r--r--  1  34520 févr. 28 14:33 LICENSE
    4 -rw-r--r--  1   1006 févr. 28 14:33 pyproject.toml
    4 -rw-r--r--  1    948 févr. 28 14:33 README.rst
    4 drwxr-xr-x  2   4096 févr. 28 14:33 scripts
    4 -rw-r--r--  1    911 févr. 28 14:33 TRANSLATORS.rst


    .
    ├── api
    │   ├── compose
    │   ├── config
    │   ├── Dockerfile
    │   ├── funkwhale_api
    │   ├── install_os_dependencies.sh
    │   ├── manage.py
    │   ├── requirements
    │   ├── requirements.apt
    │   ├── requirements.pac
    │   ├── requirements.txt
    │   ├── setup.cfg
    │   └── tests
    ├── CHANGELOG
    ├── changes
    │   ├── changelog.d
    │   ├── __init__.py
    │   ├── notes.rst
    │   └── template.rst
    ├── CONTRIBUTING.rst
    ├── CONTRIBUTORS.txt
    ├── demo
    │   ├── env.sample
    │   ├── README.md
    │   └── setup.sh
    ├── deploy
    │   ├── apache.conf
    │   ├── docker-compose.yml
    │   ├── docker.funkwhale_proxy.conf
    │   ├── docker.nginx.template
    │   ├── docker.proxy.template
    │   ├── env.prod.sample
    │   ├── FreeBSD
    │   ├── funkwhale-beat.service
    │   ├── funkwhale_proxy.conf
    │   ├── funkwhale-server.service
    │   ├── funkwhale.target
    │   ├── funkwhale-worker.service
    │   └── nginx.template
    ├── dev.yml
    ├── docker
    │   ├── nginx
    │   ├── ssl
    │   ├── traefik.toml
    │   └── traefik.yml
    ├── docs
    │   ├── api.rst
    │   ├── architecture.rst
    │   ├── build_docs.sh
    │   ├── build_swagger.sh
    │   ├── changelog.rst
    │   ├── configuration.rst
    │   ├── conf.py
    │   ├── contributing.rst
    │   ├── developers
    │   ├── Dockerfile
    │   ├── features.rst
    │   ├── federation
    │   ├── importing-music.rst
    │   ├── index.rst
    │   ├── installation
    │   ├── Makefile
    │   ├── serve.py
    │   ├── swagger.yml
    │   ├── third-party.rst
    │   ├── translators.rst
    │   ├── troubleshooting.rst
    │   ├── upgrading
    │   └── users
    ├── front
    │   ├── babel.config.js
    │   ├── Dockerfile
    │   ├── locales
    │   ├── package.json
    │   ├── public
    │   ├── scripts
    │   ├── src
    │   ├── stats.json
    │   ├── tests
    │   ├── vue.config.js
    │   └── yarn.lock
    ├── LICENSE
    ├── pyproject.toml
    ├── README.rst
    ├── scripts
    │   ├── clean-unused-artifacts.py
    │   └── set-api-build-metadata.sh
    └── TRANSLATORS.rst

    27 directories, 61 files



**dev.yml** docker-compose file
=================================

.. literalinclude:: ../dev.yml
   :linenos:


.env.dev
============


.. literalinclude:: ../.env.dev
   :linenos:
