
.. _funkwhale_docker:

===========================================
Funkwhale tutoriel about docker
===========================================


- https://dev.funkwhale.audio/funkwhale/funkwhale
- https://docs.funkwhale.audio/installation/docker.html
- https://mastodon.eliotberriot.com/@eliotberriot
- https://framablog.org/2018/09/07/funkwhale-eliot-franchit-le-pas/
- https://framablog.org/2018/05/25/funkwhale-les-baleines-melomanes-libres-et-decentralisees/


Pre-Introduction
=================

Septembre 2018
---------------

.. seealso::

   - https://framablog.org/2018/09/07/funkwhale-eliot-franchit-le-pas/

en 2017 (ou était-ce 2016 ?), j’ai découvert une nouvelle super technologie,
appelée VueJS, que j’ai utilisée pour reconstruire le front-end du projet,
à partir de zéro.

Soudain, tout est devenu à la fois plus facile et plus satisfaisant
du point de vue de l’utilisateur et du développeur, et je me sentais
assez confiant pour montrer le projet à des amis proches et à la famille.

En utilisant leurs commentaires, j’ai pu améliorer le projet et ajouter
progressivement de nouvelles fonctionnalités.
La première fois que j’en ai parlé publiquement, c’était probablement ici,
en juillet 2017.

Mais le vrai coup d’envoi du projet a été avec ce pouet, où j’ai invité
des gens sur ma propre instance pour une bêta fermée, fin février 2018.


Mai 2018
----------

.. seealso::

   - https://framablog.org/2018/05/25/funkwhale-les-baleines-melomanes-libres-et-decentralisees/


Enfin, une des grandes nouveautés de ces dernières années, c’est à mon
avis Docker qui réduit grandement les difficultés à installer un service
tel que Funkwhale.

Tout le monde n’est pas forcément convaincu par cette techno, qui a
aussi ses problèmes, mais la simplicité pour les déploiements est
quand même un atout assez fort.

Très concrètement, si tu `consultes la doc d’installation`_ de Funkwhale
sur Docker, tu pourras constater qu’il suffit d’une dizaine de commandes
à exécuter pour installer Funkwhale sur son serveur.

.. _`consultes la doc d’installation`: https://docs.funkwhale.audio/installation/docker.html


Introduction
==============


Docker is the easiest way to get a Funkwhale instance up and running.

We support two types of Docker deployments:

- Mono-container: all processes live in the same container (database,
  nginx, redis, etc.).
  It’s easier to deploy and to integrate with container management systems
  like Portainer.
  However, it’s not possible to scale this type of deployment on multiple servers.
- Multi-container: each process lives in a dedicated container.
  This setup is more involved but also more flexible and scalable.


Mono-container Dockerfile
====================================

.. seealso::

   - https://github.com/thetarkus/docker-funkwhale


::

    FROM alpine:3.8
    MAINTAINER thetarkus


    #
    # Installation
    #

    ARG arch=amd64
    RUN \
        echo 'installing dependencies' && \
        apk add                \
        shadow             \
        gettext            \
        git                \
        postgresql         \
        postgresql-contrib \
        postgresql-dev     \
        python3-dev        \
        py3-psycopg2       \
        py3-pillow         \
        redis              \
        nginx              \
        musl-dev           \
        gcc                \
        unzip              \
        libldap            \
        libsasl            \
        ffmpeg             \
        libpq              \
        libmagic           \
        libffi-dev         \
        zlib-dev           \
        openldap-dev && \
        \
        \
        echo 'creating directories' && \
        mkdir -p /app /run/nginx /run/postgresql /var/log/funkwhale && \
        \
        \
        echo 'creating users' && \
        adduser -s /bin/false -D -H funkwhale funkwhale && \
        \
        \
        echo 'downloading archives' && \
        wget https://github.com/just-containers/s6-overlay/releases/download/v1.21.7.0/s6-overlay-$arch.tar.gz -O /tmp/s6-overlay.tar.gz && \
        \
        \
        echo 'extracting archives' && \
        cd /app && \
        tar -C / -xzf /tmp/s6-overlay.tar.gz && \
        \
        \
        echo 'setting up nginx' && \
        rm /etc/nginx/conf.d/default.conf && \
        \
        \
        echo 'removing temp files' && \
        rm /tmp/*.tar.gz

    COPY ./src/api /app/api

    RUN \
        ln -s /usr/bin/python3 /usr/bin/python && \
        echo 'fixing requirements file for alpine' && \
        sed -i '/Pillow/d' /app/api/requirements/base.txt && \
        \
        \
        echo 'installing pip requirements' && \
        pip3 install --upgrade pip && \
        pip3 install setuptools wheel && \
        pip3 install -r /app/api/requirements.txt

    COPY ./src/front /app/front


    #
    # Environment
    # https://dev.funkwhale.audio/funkwhale/funkwhale/blob/develop/deploy/env.prod.sample
    # (Environment is at the end to avoid busting build cache on each ENV change)
    #

    ENV FUNKWHALE_HOSTNAME=yourdomain.funkwhale \
        FUNKWHALE_PROTOCOL=http \
        DJANGO_SETTINGS_MODULE=config.settings.production \
        DJANGO_SECRET_KEY=funkwhale \
        DJANGO_ALLOWED_HOSTS='127.0.0.1,*' \
        DATABASE_URL=postgresql://funkwhale@:5432/funkwhale \
        MEDIA_ROOT=/data/media \
        MUSIC_DIRECTORY_PATH=/music \
        NGINX_MAX_BODY_SIZE=100M \
        STATIC_ROOT=/app/api/staticfiles \
        FUNKWHALE_SPA_HTML_ROOT=http://localhost/front/

    #
    # Entrypoint
    #

    COPY ./root /
    COPY ./src/funkwhale_nginx.template /etc/nginx/funkwhale_nginx.template
    ENTRYPOINT ["/init"]



Dockerfile files
=====================

.. toctree::
   :maxdepth: 3

   api/api
   docs/docs
   front/front

Docker-compose files
======================

::

    deploy/docker-compose.yml  dev.yml  docker/traefik.yml  docs/swagger.yml



.. toctree::
   :maxdepth: 3

   deploy/deploy
   dev/dev
