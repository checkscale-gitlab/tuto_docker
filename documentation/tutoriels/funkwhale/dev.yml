version: "3"

services:
  front:
    build: front
    env_file:
      - .env.dev
      - .env
    environment:
      - "HOST=0.0.0.0"
      - "VUE_PORT=${VUE_PORT-8080}"
    ports:
      - "${VUE_PORT-8080}"
    volumes:
      - "./front:/app"
      - "/app/node_modules"
      - "./po:/po"
    networks:
      - internal

  postgres:
    env_file:
      - .env.dev
      - .env
    image: postgres:${POSTGRES_VERSION-11}
    command: postgres -c log_min_duration_statement=0
    volumes:
      - "./data/${COMPOSE_PROJECT_NAME-node1}/postgres:/var/lib/postgresql/data"
    networks:
      - internal

  redis:
    env_file:
      - .env.dev
      - .env
    image: redis:3.0
    volumes:
      - "./data/${COMPOSE_PROJECT_NAME-node1}/redis:/data"
    networks:
      - internal

  api:
    env_file:
      - .env.dev
      - .env
    build: &backend
      context: ./api
      dockerfile: Dockerfile
      args:
        install_dev_deps: 1
    entrypoint: compose/django/dev-entrypoint.sh
    command: python /app/manage.py runserver 0.0.0.0:${FUNKWHALE_API_PORT-5000}
    volumes:
      - ./api:/app
      - "${MUSIC_DIRECTORY_PATH-./data/music}:/music:ro"
    environment:
      - "FUNKWHALE_HOSTNAME=${FUNKWHALE_HOSTNAME-localhost}"
      - "FUNKWHALE_HOSTNAME_SUFFIX=funkwhale.test"
      - "FUNKWHALE_HOSTNAME_PREFIX=${COMPOSE_PROJECT_NAME}"
      - "FUNKWHALE_PROTOCOL=${FUNKWHALE_PROTOCOL-http}"
      - "DATABASE_URL=postgresql://postgres@postgres/postgres"
      - "CACHE_URL=redis://redis:6379/0"
    links:
      - postgres
      - redis
    networks:
      - internal
    cap_add:
      - SYS_PTRACE

  celeryworker:
    env_file:
      - .env.dev
      - .env
    build: *backend
    links:
      - postgres
      - redis
    command: celery -A funkwhale_api.taskapp worker -l debug -B
    environment:
      - "FUNKWHALE_HOSTNAME=${FUNKWHALE_HOSTNAME-localhost}"
      - "FUNKWHALE_HOSTNAME_SUFFIX=funkwhale.test"
      - "FUNKWHALE_HOSTNAME_PREFIX=${COMPOSE_PROJECT_NAME}"
      - "FUNKWHALE_PROTOCOL=${FUNKWHALE_PROTOCOL-http}"
      - "DATABASE_URL=postgresql://postgres@postgres/postgres"
      - "CACHE_URL=redis://redis:6379/0"
    volumes:
      - ./api:/app
      - "${MUSIC_DIRECTORY_PATH-./data/music}:/music:ro"
    networks:
      - internal
  nginx:
    command: /entrypoint.sh
    env_file:
      - .env.dev
      - .env
    image: nginx
    environment:
      - "NGINX_MAX_BODY_SIZE=${NGINX_MAX_BODY_SIZE-100M}"
      - "FUNKWHALE_API_IP=${FUNKHALE_API_IP-api}"
      - "FUNKWHALE_API_PORT=${FUNKWHALE_API_PORT-5000}"
      - "FUNKWHALE_FRONT_IP=${FUNKHALE_FRONT_IP-front}"
      - "FUNKWHALE_FRONT_PORT=${VUE_PORT-8080}"
      - "COMPOSE_PROJECT_NAME=${COMPOSE_PROJECT_NAME- }"
      - "FUNKWHALE_HOSTNAME=${FUNKWHALE_HOSTNAME-localhost}"
    depends_on:
      - api
      - front
    volumes:
      - ./docker/nginx/conf.dev:/etc/nginx/nginx.conf.template:ro
      - ./docker/nginx/entrypoint.sh:/entrypoint.sh:ro
      - "${MUSIC_DIRECTORY_PATH-./data/music}:/music:ro"
      - ./deploy/funkwhale_proxy.conf:/etc/nginx/funkwhale_proxy.conf:ro
      - "${MEDIA_ROOT-./api/funkwhale_api/media}:/protected/media:ro"
    networks:
      - federation
      - internal

    labels:
      traefik.backend: "${COMPOSE_PROJECT_NAME-node1}"
      traefik.frontend.rule: "Host:${COMPOSE_PROJECT_NAME-node1}.funkwhale.test,${NODE_IP-127.0.0.1}"
      traefik.enable: "true"
      traefik.federation.protocol: "http"
      traefik.federation.port: "80"
      traefik.frontend.passHostHeader: true
      traefik.docker.network: federation

  docs:
    build: docs
    command: python serve.py
    volumes:
      - ".:/app/"
    ports:
      - "35730:35730"
      - "8001:8001"

  api-docs:
    image: swaggerapi/swagger-ui
    environment:
      - "API_URL=/swagger.yml"
    ports:
      - "8002:8080"
    volumes:
      - "./docs/swagger.yml:/usr/share/nginx/html/swagger.yml"

networks:
  ? internal
  federation:
    external:
      name: federation
