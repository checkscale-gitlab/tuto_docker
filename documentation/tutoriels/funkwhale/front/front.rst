

.. index::
   pair: Dockerfile  ; front (Funkwhale)


.. _funkwhale_front_docker:

===========================================
Funkwhale front Dockerfile
===========================================





Dockerfile
============

.. literalinclude:: Dockerfile
   :linenos:


package.json
=============

.. literalinclude:: package.json
   :linenos:


scripts/i18n-compile.sh
==========================

.. literalinclude:: scripts/i18n-compile.sh
   :linenos:


scripts/i18n-extract.sh
==========================

.. literalinclude:: scripts/i18n-extract.sh
   :linenos:


scripts/i18n-weblate-to-origin.sh
===================================

.. literalinclude:: scripts/i18n-weblate-to-origin.sh
   :linenos:


vue.config.js
=================

.. literalinclude:: vue.config.js
   :linenos:

babel.config.js
=================

.. literalinclude:: babel.config.js
   :linenos:
