.. index::
   pair: Julia Evans ; Containers
   pair: cgroups ; processes
   pair: overlay ; filesystems

.. containers_julia_evans:

=============================================
**How Containers Work !** by Julia Evans
=============================================

.. seealso::

   - https://wizardzines.com/zines/containers/





Introduction
===============

When you first start using containers, they seem really weird.

Is it a process? A virtual machine? What’s a container image?

Why isn’t the networking working?

I’m on a Mac but it’s somehow running Linux sort of?
And now it’s written a million files to my home directory as root?
What’s HAPPENING?

And you’re not wrong. Containers seem weird because they ARE really weird.

They’re not just one thing, they’re what you get when you glue together
6 different features that were mostly designed to work together but
have a bunch of confusing edge cases.

This zine explains EXACTLY what happens when you run a container on
your computer.

It turns out that there are only a few big ideas you need to understand
what’s going on (images! overlay filesystems! capabilities! pivot_root!
cgroups! namespaces! seccomp-bpf!).

Once you understand these fundamentals, you’ll be able to go from “um,
my container got killed??? why??? help!!!” to “oh, we set the cgroup
memory limit too low! I’ll just look up how to adjust it!”

cgroups
========

.. figure:: cgroups.jpeg
   :align: center


.. figure:: overlay-filesystems.jpeg
   :align: center
