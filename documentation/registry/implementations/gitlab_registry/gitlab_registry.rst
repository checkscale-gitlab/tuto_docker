
.. index::
   pair: Gitlab ; Registry
   pair: Container ; Registry


.. _gitlab_container_registry:

==================================
Gitlab Container Registry
==================================

.. seealso::

   - https://docs.gitlab.com/ee/user/project/container_registry.html
   - https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#using-the-gitlab-container-registry




Historique
============

2016-05-23 : GitLab Container Registry
-----------------------------------------


.. seealso::

   - https://about.gitlab.com/2016/05/23/gitlab-container-registry/


Introduction
--------------

Yesterday we released GitLab 8.8, super powering GitLab's built-in
continuous integration. With it, you can build a pipeline in GitLab,
visualizing your builds, tests, deploys and any other stage of the life
cycle of your software. Today (and already in GitLab 8.8), we're
releasing the next step: GitLab Container Registry.

GitLab Container Registry is a secure and private registry for Docker
images. Built on open source software, GitLab Container Registry isn't
just a standalone registry; it's completely integrated with GitLab.

GitLab is all about having a single, integrated experience and our
registry is no exception. You can now easily use your images for
GitLab CI, create images specific for tags or branches and much more.

Our container registry is the first Docker registry that is fully
integrated with Git repository management and comes out of the box
with GitLab 8.8. So if you've upgraded, you already have it!
This means our integrated Container Registry requires no additional
installation. It allows for easy upload and download of images
from GitLab CI. And it's free.


Docker Basics
---------------

The main component of a Docker-based workflow is an **image**, which contains
everything needed to run an application.
Images are often created automatically as part of continuous integration,
so they are updated whenever code changes. When images are built to be
shared between developers and machines, they need to be stored somewhere,
and that's where a container registry comes in.

The registry is the place to store and tag images for later use.
Developers may want to maintain their own registry for private, company
images, or for throw-away images used only in testing.

Using GitLab Container Registry means you don't need to set up and administer
yet another service, or use a public registry.


Summary
---------

GitLab Container Registry is the latest addition to GitLab's integrated
set of tools for the software development life cycle and comes with
GitLab 8.8 and up.

With GitLab Container Registry, testing and deploying Docker containers
has never been easier. GitLab Container Registry is available on-premises
in GitLab CE and GitLab EE at no additional cost and installs in the
same infrastructure as the rest of your GitLab instance.

Container Registry is enabled on GitLab.com; it's completely free, and
you can start using it right now!


Administration
=================

.. seealso::

   - https://docs.gitlab.com/ce/administration/container_registry.html



Examples
==========


.. toctree::
   :maxdepth: 3

   examples/examples
