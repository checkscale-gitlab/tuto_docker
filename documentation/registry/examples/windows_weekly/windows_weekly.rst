
.. index::
   pair: Docker Registry; examples


.. _running_own_registry:

===========================
Running Your Own Registry
===========================

.. seealso::

   - https://blog.sixeyed.com/windows-weekly-dockerfile-20-running-your-own-registry/
