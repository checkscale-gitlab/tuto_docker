
.. index::
   pair: Docker ; Registry


.. _docker_registry:

===========================
Registry
===========================

.. seealso::

   - https://docs.docker.com/registry/introduction/




Definition
=============


The registry is the "ship" part of the build, ship, run workflow.

You package your app in a Docker image using a Dockerfile and
docker image build, and the output is an image on your machine
(or the CI server that ran the build).

To make the image available to other users, you ship it to a registry
with docker image push.
**The default registry is Docker Hub, which is a free public registry service**.

If you want to keep your images private, so they're only accessible
within your own network, you can use a commercial registry like Docker
Trusted Registry - which also provides security scanning and image signing.


Understanding image naming
==============================

.. seealso::

   - https://docs.docker.com/registry/introduction/

Image names as used in typical docker commands reflect their origin:

- **docker pull** ubuntu instructs docker to pull an image named ubuntu
  from the official Docker Hub.
  This is simply a shortcut for the longer docker pull docker.io/library/ubuntu command
- docker pull myregistrydomain:port/foo/bar instructs docker to contact
  the registry located at myregistrydomain:port to find the image foo/bar

Use cases
===========

Running your own Registry is a great solution to integrate with and
complement your CI/CD system.
In a typical workflow, a commit to your source revision control system
would trigger a build on your CI system, which would then push a new
image to your Registry if the build is successful.
A notification from the Registry would then trigger a deployment on a
staging environment, or notify other systems that a new image is available.

It’s also an essential component if you want to quickly deploy a new
image over a large cluster of machines.

Finally, it’s the best way to distribute images inside an isolated network.
Requirements

You absolutely need to be familiar with Docker, specifically with regard
to pushing and pulling images.
You must understand the difference between the daemon and the cli, and
at least grasp basic concepts about networking.

Also, while just starting a registry is fairly easy, operating it in a
production environment requires operational skills, just like any other
service.
You are expected to be familiar with systems availability and scalability,
logging and log processing, systems monitoring, and security 101.

Strong understanding of http and overall network communications, plus
familiarity with golang are certainly useful as well for advanced
operations or hacking.


Implementations
==================

.. toctree::
   :maxdepth: 3

   implementations/implementations

Examples
===========

.. toctree::
   :maxdepth: 3

   examples/examples
