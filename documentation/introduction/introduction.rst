
.. index::
   pair: Introduction ; Docker
   pair: Définitions ; Docker
   pair: Définitions ; Devops
   pair: Définitions ; Agilité
   pair: Virtualisation ; légère
   pair: MISC ; Janvier/Février 2018
   pair: MISC ; Docker


.. _intro_docker:

=======================
Introduction à Docker
=======================

.. figure:: ../../images/Docker_logo.svg.png
   :align: center

   Le logo Docker


.. seealso::

   - https://fr.wikipedia.org/wiki/Docker_(logiciel)
   - https://www.docker.com/
   - https://twitter.com/docker
   - https://en.wikipedia.org/wiki/Cloud_computing
   - https://fr.wikipedia.org/wiki/Virtualisation
   - https://en.wikipedia.org/wiki/Devops
   - https://docs.docker.com/get-started/

.. contents::
   :depth: 5




Pourquoi utiliser docker ?
==============================

Transformation de la DSI des entreprises
------------------------------------------

.. seealso::

   - https://actu.alfa-safety.fr/service-aws/devops-et-transformation-dela-dsi/

Trois évolutions majeures convergent depuis peu et poussent à une
transformation de la DSI des entreprises:

- la pression du time to market : l’accélération du rythme d’évolution
  des applications, en particulier web, pour sortir au plus vite de
  nouvelles fonctionnalités et répondre aux besoins du marché
- Le Devops : pour livrer plus vite, les équipes de Dev font évoluer
  leurs méthodes pour rendre les déploiements plus rapides, fréquents
  et fluides, et attendent que l’infrastructure, les « Ops » évoluent
  au même rythme.
- le cloud public : il a atteint un niveau de maturité et d’efficacité
  tel qu’une majorité des DSI travaille maintenant à l’intégrer, souvent
  sous la pression des équipes de développement,


.. figure:: DevOps-Adoption-from-RightScale-DevOps-Trends-Report.png
   :align: center



Pour donner davantage d’autonomie aux développeurs
----------------------------------------------------

.. seealso::

   - https://actu.alfa-safety.fr/devops/docker-en-production/

Avec Docker, donnez davantage d’autonomie aux développeurs

L’un des atouts du conteneur est de donner davantage d’autonomie
au développeur.
Ce dernier doit pouvoir travailler sur son application sans se soucier
de la configuration de la machine sur laquelle il travaille : il doit
pouvoir développer sur son poste de travail et pousser son conteneur
sur un serveur de test, puis pré-production, et jusqu’en production
sans rencontrer de difficultés.

Le développeur doit aussi pouvoir modifier son docker et en gérer les
versions sans se préoccuper des conséquences pour la production.

En résumé, un des bénéfices du conteneur c’est qu’il doit pouvoir se
déployer n’importe où en toute sécurité.




Faire évoluer son système d'information
------------------------------------------

.. seealso::

   - https://linuxfr.org/forums/linux-general/posts/docker-en-prod


Bonjour à tous, après la virtualisation il y a docker (qui a le vent en poupe).
Je me dis qu'il y a peut-être quelque chose à faire.
Le concept est assez simple, l'utilisation a l'air souple.

Comme par hasard je dois migrer le serveur intranet de ma boite,
actuellement il est en RHE 5.x et depuis la version 6.5 docker
est intégré par RedHat. Il sert à plusieurs choses :

- dev pour les sites internet;
- PIM interne
- Cacti
- …

J'aimerais bien avoir un environnement qui me permette d'ajouter
Ruby par exemple sans tout péter sur les autres devs, ou installer
la version de php 7 alors que le reste doit rester en php 5,
la lib rrdtool 1.4 alors qu'un autre doit rester en 1.2…
Enfin le genre de chose **bien prise de tête à gérer**.

Après avoir lu pas mal de doc autres que celles de RH je me rend compte
qu'à chaque fois se sont des environnements de dev qui sont mis en place
mais jamais de la prod, du vrai, du concret, avec du users bien bourrin.

Avez-vous des exemples ou des expériences (réussi ou pas) d'archi en prod ?


Pour que ça fonctionne aussi sur une autre machine
-----------------------------------------------------

.. seealso::

   - http://putaindecode.io/fr/articles/docker/


Il était une fois un jeune développeur qui codait tranquillement sur son
ordinateur. Il était pressé car comme tout étudiant qui se respecte il
devait présenter son travail le lendemain matin.
Après des heures de travail, l'application était là, et elle fonctionnait
à merveille ! Le lendemain, notre codeur arriva tout fier pour sa
présentation, avec son projet sur une clé usb.
Il le transfère sur l'ordinateur de son pote et là, ça ne fonctionne pas !

Quel est le problème ?

L'application de notre jeune développeur ne fonctionne pas sur
l'ordinateur de son ami à cause d'un problème d'environnement.
Entre deux systèmes, il peut y avoir des différences de version
sur les dépendances ou encore des bibliothèques manquantes.



Livre blanc Ubuntu
--------------------

:download:`ubuntu/WP_The_no-nonsense-way-to-accelerate-your-business-with_containers.pdf`


Définitions concernant l'agilité et le mouvement **Devops**
==============================================================

Définition de **Devops** p.34 Programmez! p.214 janvier 2018
--------------------------------------------------------------

.. seealso::

   - https://en.wikipedia.org/wiki/Devops
   - http://david.monniaux.free.fr/dotclear/index.php/post/2018/01/05/Pourquoi-l-informatique-devient-incompr%C3%A9hensible%2C-et-l-impact-sur-la-s%C3%A9curit%C3%A9

Si le mouvement **Devops** fait bien référence à l'automatisation des
tests unitaires ou fonctionnels avec la mise en place de l'intégration
continue ou à l'automatisation, ce n'est pas l'aspect principal
qu'évoque le mouvement **Devops**.

Le **Devops** est un mouvement qui privilégie la mise en place d'un alignement
de l'ensemble de la DSI autour **d'objectifs communs**;
le terme **Devops** est la concaténation de dev pour développeur
et ops pour opérationnels, soit les ingénieurs responsables des
infrastructures.

Avoir une équipe enfermée dans une pièce totalement isolée des équipes
de développement pour mettre en place des solutions d'intégration
continue ou de livraison continue ne correspond pas à ce concept **Devops**.
C'est pourtant cette façon de faire que nous voyons de plus en plus
aujourd'hui.

Définition 2, Le **Devops** pour répondre à l’appel de l’innovation 2018-01-04
----------------------------------------------------------------------------------

.. seealso::

   - https://www.programmez.com/avis-experts/le-**Devops**-pour-repondre-lappel-de-linnovation-26954

Le **Devops** est axé sur la collaboration, nécessaire pour développer,
tester et déployer des applications rapidement et régulièrement.

C'est un changement culturel, qui met l'accent sur le renforcement de la
communication et de la collaboration entre différentes équipes, telles
que celles chargées du développement, de l'exploitation et de
l'assurance-qualité.

L'objectif est de décloisonner les services qui composent une
organisation afin de créer un lieu de travail plus collaboratif et de
créer ainsi une synergie qui, en bout de chaîne, profite à l'utilisateur
final.
Car c’est un fait avéré, la création et la conservation de relations
solides avec les clients offrent des avantages exponentiels, dont une
diminution de la perte de clientèle et des sources de revenus
potentiellement plus nombreuses.

Car le **Devops** est avant tout un concept, il n'existe par UN outil de
**Devops** à proprement parler, mais un faisceau d'outils permettant
d'instaurer et d'entretenir une culture **Devops**.
Il regroupe à la fois des outils open source et propriétaires dédiés à
des tâches spécifiques dans les processus de développement et de
déploiement.

D'ailleurs, en parlant de processus, évoquons un instant le déploiement continu.

Le déploiement continu repose entièrement sur des processus, et
l'automatisation y joue un rôle clé. Les processus de déploiement
continu sont l'un des éléments fondamentaux d'une transformation **Devops**.
Le déploiement continu et le **Devops** permettent aux équipes de
développement d'accélérer considérablement la livraison de logiciels.
Grâce aux processus de déploiement continu et à la culture **Devops**,
les équipes peuvent offrir en permanence du code sûr, testé et prêt à
être utilisé en production.
Cela inclut la publication de mises à jour logicielles, ce qui, dans
une entreprise de télécommunication, peut parfois survenir trois fois
par jour, voire plus.



Définition 3, extrait p.53 MISC N95, Janvier/février, 2018, **"Ne pas prévoir, c'est déjà gémir"**
------------------------------------------------------------------------------------------------------


L’ère des hyperviseurs est-elle révolue ?
La bataille commerciale autour de la sécurité et de la performance persiste-t-elle ?

C’est à présent un conflit dépassé, car la sécurité est prise en compte
désormais dans les conteneurs au niveau des prérequis.

L’importance du choix de la sécurité réside davantage dans
l’édifice construit et son évolution.

Il devient évident que la **virtualisation légère** va gagner du terrain,
les hyperviseurs vont alors devenir obsolètes et c’est dans ce contexte
qu’il fait repenser l’action des équipes de sécurité.

En faisant avancer les vrais échanges entre Dev et Ops, le **Devops** a
changé la donne et la **production bénéficie enfin de l’agilité prônée
depuis quelques années**.

En intégrant la sécurité dans le **SecDevops**, et en s’assurant d’avoir des
composants sécurisés au maximum, l’aspect sécuritaire devient alors une
composante à valeur ajoutée pour la production.

Certains pensent qu’utiliser les systèmes qui ont fait leur preuve dans
le temps serait le gage d’une sécurité beaucoup plus fiable et plus
simple à mettre en œuvre.

Il semble aujourd’hui de plus en plus évident pour un responsable de
systèmes d’information que manquer ce tournant de la technologie des
conteneurs, serait une **assurance d’être rapidement mis à l’écart des
évolutions en cours**.


Citations
+++++++++++


Ne pas prévoir, c'est déjà gémir
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**"Ne pas prévoir, c'est déjà gémir"**
Léonard de vinci.


La vie, c’est comme une bicyclette, il faut avancer pour ne pas perdre l’équilibre
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**La vie, c’est comme une bicyclette, il faut avancer pour ne pas perdre l’équilibre**
Albert Einstein


Devops, intégration et déploiement continus, pourquoi est-ce capital et comment y aller ?
--------------------------------------------------------------------------------------------

.. seealso::

   - https://actu.alfa-safety.fr/devops/devops-integration-et-deploiement-continus-pourquoi-est-ce-capital-et-comment-y-aller/

.. figure:: integration_continue.png
   :align: center

   Intégration continue


« Intégration continue » (CI), « déploiement continu » (CD),  « Devops »,
autant de  termes que l’on entend très fréquemment dès que l’on parle
d’applications Web et de transformation numérique, et pourtant ce
sont des concepts encore mal connus dans leur mise en œuvre.

De quoi s’agit-il ? Tout simplement d’assurer la sortie de nouvelles
fonctionnalités d’une application sur un rythme beaucoup plus régulier
et rapide.

Traditionnellement, un rythme de déploiement standard sur une
application classique est d’une à deux versions majeures par an.
Pour chaque version majeure, on regroupe un ensemble de nouvelles
fonctionnalités, ce qui donne délai de 6 à 12 mois entre deux nouveautés.

Entretemps, on se contente de corriger les bugs, de sortir des versions
mineures. C’est terriblement long, surtout à l’ère d’internet.
L’objectif est d’assurer la cohérence des évolutions, regrouper les
testss, sécuriser la production et limiter les migrations pour les
clients, mais cela pénalise les délais.

Ce délai s’explique par le fait que c’est un processus séquentiel,
impliquant différentes équipes et qu’à chaque étape, il faut
synchroniser les acteurs, faire des demandes, les planifier,
tout cela générant des délais.

Le déploiement continu prend le contrepied et permet d’permet d’accélérer ce rythme en :

- découpant les versions en un plus grand nombre de livraisons de moindre
  taille et moins complexes à tester,
- automatisant au maximum les étapes de tests et passages en production
  d’une nouvelle version afin de réduire les cycles,
- permettant un déploiement très régulier des nouveautés.



Agilité et **Devops**: Extrait p. 35 de [Programmez!] , N°214, janvier 2018
------------------------------------------------------------------------------

.. seealso::

   - https://www.programmez.com/magazine/article/agilite-developpeurs/**Devops**-une-bonne-idee

Les développeurs **doivent** évoluer pour suivre ces deux mouvements
populaires (Agilité + **Devops**) qui se déploient très rapidement au sein
de l'ensemble des DSI françaises.
L'agilité et le **Devops** sont de très bonnes évolutions tant elles
apportent aux DSI et au produit final.


.. _devops_engineer:

What is a DevOps Engineer ?
------------------------------

.. seealso:: http://blog.shippable.com/how-to-be-a-great-devops-engineer

A major part of adopting DevOps is to create a better working relationship
between development and operations teams.

Some suggestions to do this include seating the teams together,
involving them in each other's processes and workflows, and even
creating one cross-functional team that does everything.

In all these methods, Dev is still Dev and Ops is still Ops.

The term DevOps Engineer tries to blur this divide between Dev and Ops
altogether and suggests that the best approach is to hire engineers
who can be excellent coders as well as handle all the Ops functions.

In short, a DevOps engineer can be a developer who can think with an
Operations mindset and has the following skillset:

- Familiarity and experience with a variety of Ops and Automation tools
- Great at writing scripts
- Comfortable with dealing with frequent testing and incremental releases
- Understanding of Ops challenges and how they can be addressed
  during design and development
- Soft skills for better collaboration across the team


According to Amazon CTO Werner Vogels::

    Giving developers operational responsibilities has greatly enhanced
    the quality of the services, both from a customer and a technology
    point of view.
    The traditional model is that you take your software to the wall
    that separates development and operations, and throw it over and
    then forget about it. Not at Amazon. You build it, you run it.
    This brings developers into contact with the day-to-day operation
    of their software. It also brings them into day-to-day contact with
    the customer. This customer feedback loop is essential for improving
    the quality of the service.

It is easier than ever before for a developer to move to a DevOps role.
Software delivery automation is getting better every day and DevOps
platforms like Shippable are making it easy to implement automation
while also giving you a Single Pane of Glass view across your entire
CI/CD pipeline.

Can an Ops engineer move to a DevOps role? Definitely, but it can be a
little more challenging since you will need to learn design and
programming skills before making that transformation. However, with the
upsurge in number of coding bootcamps, it is probably an easier
transition to make than it was a few years ago. Ops engineers can bring
much needed insights into how software design can cause Ops challenges,
so once you get past the initial learning curve for design/coding,
you're likely to become a valued DevOps engineer.



Définitions concernant Docker
==============================

.. seealso::

   - https://www.docker.com/what-docker


Définition de Docker sur Wikipedia en français
------------------------------------------------

Docker est un logiciel libre qui automatise le déploiement d'applications
dans des conteneurs logiciels.
Selon la firme de recherche sur l'industrie 451 Research::

    Docker est un outil qui peut empaqueter une application et ses
    dépendances dans un conteneur isolé, qui pourra être exécuté sur
    n'importe quel serveur.

Ceci permet d'étendre la flexibilité et la portabilité d’exécution
d'une application, que ce soit sur la machine locale, un cloud privé ou
public, une machine nue, etc


Docker est "agile"
---------------------


Améliorations des temps de développement et de déplpoiement par 13.


.. figure:: docker_agilite.png
   :align: center

   Source: https://www.docker.com/what-docker


Docker est portable
--------------------


Docker est portable ce qui permet d'avoir des environnements
de développement, test et production pratiquement identiques.


.. figure:: docker_portable.png
   :align: center

   Source: https://www.docker.com/what-docker


Docker est sécurisé
--------------------


.. figure:: docker_securite.png
   :align: center

   Source: https://www.docker.com/what-docker


Les conteneurs Docker sont plus légers et rapides que les machines virtuelles
-------------------------------------------------------------------------------


.. figure:: virtual_machines_versus_conteneurs.png
   :align: center

   Source: https://www.docker.com/what-container


Containers
+++++++++++

Containers are an abstraction at the app layer that packages code and
dependencies together.
Multiple containers can run on the same machine and share the OS kernel
with other containers, each running as isolated processes in user space.

Containers take up less space than VMs (container images are typically
tens of MBs in size), and start almost instantly.

Virtual machines (VMs)
++++++++++++++++++++++++

Virtual machines (VMs) are an abstraction of physical hardware turning
one server into many servers.

The hypervisor allows multiple VMs to run on a single machine.

Each VM includes a full copy of an operating system, one or more apps,
necessary binaries and libraries - taking up tens of GBs.

VMs can also be slow to boot.



Docker can run your applications in production at **native speed**
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Source: p.255 du livre "Python Microservices Development" de Tarek Ziadé.


::

    ...
    that is where VMs are a great solution to run your applications.
    ...


In the past ten years, many software projects  that required an
elaborate setup to run started to provide read-to-run VMs, using tools
such as VMWare or VirtualBox.
Those VMs included the whole stack, like prefilled databases.
Demos became easyly runnable on most platforms with a single command.
That was progress.

However, some of those tools were not fully open source virtualization
tool and they were very slow to run, and greedy in memory and CPU
and terrible with disk I/O. It was **unthinkable** to run them in
production, and they were mostly used for demos.

The **big revolution** came with Docker, an open source virtualization
tool, which wa first released in 2013, and became hugley popular.
Moreover, unlike VMWare or VirtualBox, Docker can run your applications
in production at **native speed**.



Dossier Docker dans le dossier MISC N°95 de janvier/février 2018
==================================================================

.. seealso::

   - https://www.miscmag.com/misc-n95-references-de-larticle-docker-les-bons-reflexes-a-adopter/



.. _docker:  https://www.miscmag.com/misc-n95-references-de-larticle-docker-les-bons-reflexes-a-adopter/
