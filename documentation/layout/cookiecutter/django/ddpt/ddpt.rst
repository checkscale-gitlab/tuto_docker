
.. index::
   pair: ddpt ; docker
   pair: Docker Swarm; docker
   ! ddpt


.. _docker_cookicutter_ddpt:

========================================================================================================
cookicutter ddpt : template for Django Projects - From development to production with **Docker Swarm**
========================================================================================================

- :ref:`physical_architecture`
- :ref:`compose_django`
- https://github.com/douglasmiranda/ddpt
