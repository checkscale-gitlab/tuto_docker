
.. index::
   pair: cookiecutter ; Django

.. _docker_cookiecutter_django:

==================================
cookiecutter docker django
==================================

.. seealso::

   - :ref:`physical_architecture`

.. toctree::
   :maxdepth: 6

   ddpt/ddpt
