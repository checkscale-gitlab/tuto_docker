

.. index::
   pair: Sebastián ; Ramírez
   pair: Full stack ; Sebastián Ramírez
   pair: Docker swarm  ; Sebastián Ramírez
   pair: swarm  ; rocks
   pair: Docker ; swarm
   ! Sebastián Ramírez


.. _sebastian_ramirez_docker:

==============================
Sebastián Ramírez (tiangolo)
==============================

.. seealso::

   - https://twitter.com/tiangolo
   - https://tiangolo.netlify.com/projects/
   - https://github.com/tiangolo/medium-posts


.. figure:: sebatian_ramirez.jpg
   :align: center

   Sebastián Ramírez




Docker projects
==================

.. seealso::

   - https://tiangolo.netlify.com/projects/


.. _docker_swarm_rocks:

Docker swarm rocks
=====================

.. seealso::

   - https://github.com/tiangolo/dockerswarm.rocks
   - https://dockerswarm.rocks/


Docker Swarm mode rocks! Ideas, tools and recipes.

Get a production-ready, distributed, HTTPS served, cluster in minutes,
not weeks.


uwsgi-nginx-flask-docker
==========================

- https://github.com/tiangolo/uwsgi-nginx-flask-docker

uwsgi-nginx-docker
===================

- https://github.com/tiangolo/uwsgi-nginx-docker


uvicorn-gunicorn-machine-learning-docker
==========================================

- https://github.com/tiangolo/uvicorn-gunicorn-machine-learning-docker

python-machine-learning-docker
===============================

- https://github.com/tiangolo/python-machine-learning-docker

Multistaging
==============

.. seealso::

   - https://medium.com/@tiangolo/angular-in-docker-with-nginx-supporting-environments-built-with-multi-stage-docker-builds-bb9f1724e984
