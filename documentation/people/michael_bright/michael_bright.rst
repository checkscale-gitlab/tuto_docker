

.. index::
   pair: Mickael ; Bright


.. _mickael_bright:

==============================
Mickael Bright
==============================

.. seealso::

   - https://github.com/mjbright
   - https://medium.com/@mjbrightfr
   - https://twitter.com/mjbright
   - https://mjbright.github.io/Talks/
   - https://mjbright.blogspot.com



.. figure:: mickael_bright.png
   :align: center





Activités septembre 2018 à Grenoble
=======================================

.. seealso::

   - :ref:`kubernetes_2018_09`
