

.. index::
   pair: Bret ; Fischer
   pair: People ; Bret Fischer


.. _bret_fischer:

==============================
Bret Fischer
==============================

.. seealso::

   - https://twitter.com/BretFisher
   - https://github.com/BretFisher/ama/issues


News
=====

2018
-----

.. seealso::

   - :ref:`swarm_single_node`
