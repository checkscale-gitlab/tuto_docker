

.. index::
   pair: Docker ; Doc (Aquasec)
   pair: Docker ; Aquasec
   pair: Security ; Aquasec


.. _docker_aquasec:

==============================
Docker aquasec documentation
==============================

.. seealso::

   - https://www.aquasec.com/wiki


About this Site
==================

This website brings together thousands of online resources about container
technology.

Containers are nothing new: as early as 1982 Unix administrators could
launch isolated processes, similar to today's containers, using the
chroot command.

The first modern container was probably Linux-VServer released in 2001.

Containers matured considerably in the 12 years that followed, until the
rise of Docker which finally took containers to the mainstream.

Today cloud computing, deployment, DevOps and agile development are
almost synonymous with containers.
So much has been written on this complex subject, and few have attempted
to organize this corpus into a meaningful format.

At Aqua Security, a pioneer in container security, we took upon ourselves
to fill this gap and collect the most important writings about container
technology - from conceptual articles and best practices to vendor
information and how to guides - to help the growing community make sense
of the space.
The end result will include over 200 sub-topics around containers,
container platforms, container orchestration and more.
With a special focus on Docker and Kubernetes which are becoming
ubiquitous in modern container setups.
