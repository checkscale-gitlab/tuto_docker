
.. index::
   pair: docker ; stack

.. _docker_stack:

==============================
**docker stack**
==============================

- https://docs.docker.com/engine/reference/commandline/stack/



Use cases
===========

::

    ssh XXXX
    mdp: ****
    su -
    mdp: *****
    dans /opt/id3/ y'a les différentes stack
    docker stack deploy --compose-file docker-compose.yml --with-registry-auth intranet
    Permet de déployer l'intranet
    docker stack ps intranet
    Pour voir si tout fonctionne
    docker stack rm intranet
    pour effacer la stack intranet


.. _docker_stack_deploy:

docker stack deploy
===================


https://docs.docker.com/engine/reference/commandline/stack_deploy/


Description
-------------

**Deploy a new stack or update an existing stack**

API 1.25+  The client and daemon API must both be at least 1.25 to use
this command.

Use the docker version command on the client to check your client and
daemon API versions.


.. _docker_stack_ls:

**docker stack ls**
====================================

- https://docs.docker.com/engine/reference/commandline/stack_ls/

Description
-------------

**List stacks.**

The client and daemon API must both be at least 1.25 to use this command.

Use the docker version command on the client to check your client and
daemon API versions.


.. _docker_stack_ps:

docker stack ps
===================


- https://docs.docker.com/engine/reference/commandline/stack_ps/


Description
---------------

**List the tasks in the stack**

The client and daemon API must both be at least 1.25 to use this command.

Use the docker version command on the client to check your client and
daemon API versions.


.. _docker_stack_rm:

docker stack rm
===================


- https://docs.docker.com/engine/reference/commandline/stack_rm/


Description
---------------

**Remove one or more stacks**

API 1.25+  The client and daemon API must both be at least 1.25 to use
this command.

Use the docker version command on the client to check your client and
daemon API versions.
